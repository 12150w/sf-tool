Metadata
====================================================================

Dependencies
--------------------------------------------------------------------

Metadata can be merged from one or more project dependencies. Dependent projects must be valid SalesForce Tool projects. Dependencies must be located in a folder in the project root folder named `dependencies`. Dependent projects can also contain dependent projects themselves. The dependencies folder can contain more than 1 dependent project, however, if they contain the same metadata or source files, then SalesForce Tool will throw an error.

Sample directory:

```text
\---top_level_project
    +---dependencies
    |    +---dependent_project_1
    |    |    +---sf
    |    |    |   \---classes
    |    |    |       +---SomeClass.cls
    |    |    |        \---SomeClass-meta.json
    |    |    \---dependencies
    |    |        \---dependent_project_1a
    |    |            \---classes
    |    |                +---SomeClass.cls
    |    |                \---SomeClass-meta.json
    |    \---dependent_project_2                // this is ok because it doesn't have the same metadata as dependent_project_1
    |         \---sf
    |             \---objects
    |                 +---Some_Object__c.json
    |                 \---CustomField
    |                     \---Some_Field__c.json
    +---sf
        +---classes
        |    +---SomeClass.cls
        |    \---SomeClass-meta.json
        \---objects
            +---Some_Object__c.json
            \---CustomField
                \---Some_Field__c.json
```

Metadata files are merged from the lowest level dependency up to the top level. That means that top level metadata files only need to contain differences from the dependent metadata file. Content files (classes, pages, etc.) don't merge. The highest level content file is used when saving. Using the directory above, these files:

`/top_level_project/sf/objects/Some_Object__c/CustomField/Some_Field__c.json`
```json
{
    "label": "Some New Field Label",
    "length": "15"
}
```

`/top_level_project/dependencies/dependent_project_1/sf/objects/Some_Object__c/.json`
```json
{
	"fullName": "Some_Field__c",
	"externalId": "false",
	"label": "Some Field",
	"length": "255",
	"required": "false",
	"trackTrending": "false",
	"type": "Text",
	"unique": "false"
}
```

would produce this JSON:

```json
{
	"fullName": "Some_Field__c",
	"externalId": "false",
    "label": "Some New Field Label",
    "length": "15",
	"required": "false",
	"trackTrending": "false",
	"type": "Text",
	"unique": "false"
}
```

When merging metadata, nested objects are merged. Nested arrays are not, unless they are defined as a sub-metadata array in a compound metadata.


SalesForce Metadata Types
-----------------------------------------------------------------------------

All SalesForce metadata types extend from one of the 4 base metadata types. Their definitions are created using the [build-types script](scripts/build-types.js) and are stored in `/src/types`. The `StaticResource` metadata type is stored in `/src/types-static`.


Static Metadata Types
-----------------------------------------------------------------------------

All static metadata types extend the `StaticResource` metadata type, which extends the `ContentMetadata` base type. These are source files that are saved in SalesForce as static resources. By default, the content file is store with the extension `.resource`.

There are 3 types that extend `StaticResource`. Each one does not use the `.resource` content file. Instead, they read from files defined by their method overrides and build the content file when saving. 

SalesForce static resources are limited to 5 MB in size and a total of 250 MB of static resources can be stored in SalesForce.

| Name | Description |
|------|-------------|
| SFZipFile | Reads/Writes data from a folder in `project/sf/resources`. Creates a zip file static resource in SalesForce. Zip file must contain at least 1 file to save. |
| [SFEmberApp](docs/ember.md) | Reads data from `project/ember`. Creates a JavaScript file static resource in SalesForce. The static resource is never stored on the disk. |
