Ember Applications
====================================================================

The SalesForce Tool allows Ember applications to easily be built for SalesForce. Ember applications are stored separately from metadata in the `/ember` directory in the project root directory.

The SalesForce tool uses the [level2-ember](https://www.npmjs.com/package/level2-ember) compiler to compile the source code into a single javascript file that is saved as a static resource in SalesForce.

> :warning: In order to easily create applications, any examples past this point require  [slc-ember](https://git.starlightconsultants.com/starlight/slc-ember.git) as a submodule dependency in the `/ember/dependencies` directory.



Directory Structure
-------------------------------------------------------------------

### SF Tool Project Structure

Each static resource corresponds to the folder in `/ember`. For example, if you have the the following directory:

```text
\---project
    +---ember
	|    \---TestEmberApp
	|	    +---app.js
	|	    +---router.js
	\---sf
	    \---staticResources
		    \---TestEmberApp-meta.json
```

You can save the static resource `TestEmberApp.js` to SalesForce with the command:

```bash
cd project
sf save SFEmberApp:TestEmberApp
```

Each folder in the `/ember` directory represents a single Ember application. The only exception is the `dependencies` folder. This folder can optionally be used to reference git submodules. The compiler reads from the `/ember` folder in the submodule root folder.


### Ember Application Directory Structure

At minimum, the root Ember application folder must contain `app.js` and `router.js`. 

```javascript
define('app', ['l2:app'], function(App) {
	return App.create({
		rootElement: '#ember-app'
	});
});
```

```javascript
define('router', ['app'], function(app) {
	app.Router.map(function() {
		// Add routes with this.route();
	});
});
```

Ember controllers, routes and components can be located anywhere in the Ember application directory because the compiler uses the name in the definition to manage dependencies. If you have several routes, controllers and components, you can organize them however you need to.

Templates, however, are dependent on folder structure. In each Ember application directory, all templates must go in the `/templates` directory.

For the given router:

```javascript
define('router', ['app'], function(app) {
	app.Router.map(function() {
		this.route('notes', { path: '/' }, function() {
			this.route('details', { path: 'details/:note_id' });
		});
	});
});
```

templates must be placed in the corresponding directory structure:

```
\---ember-app
    +---app.js
    +---router.js
	|
	... (other application files)
	\---templates
	    +---notes.hbs
		\---notes
		    \---notes-details.hbs
```

All component templates must be in a `/components` folder in the `/templates` folder.


Resolver
----------------------------------------------------------------------------

The dependency resolver uses an AMD module pattern. For example:

```javascript
define('myNamespace:myModule', ['someDependency'], function(Dependency) {
	return function() {
		var dep = new Dependency();
		
		// and so on...
	};
});
```

The resolver uses the same syntax as the Ember resolver except for components. The names are dasherized to match the their use in a handlebars template. For example:

```javascript
define('component:my-component', function() {
	// component definition
});
```

can be used in a template like this:

```handlebars
{{my-component}}
```


Ember in VisualForce
------------------------------------------------------------------------

To include Ember applications in SalesForce, just include the static resource. In order to run the application, jquery.js and ember.js must be included in some form (in this order). The best method is to add all vendor dependencies to a folder and save them as a static resource.

```html
<apex:page>
	<!-- Dependencies -->
	<apex:includeScript value="{!URLFOR($Resource.Vendor, 'jquery/1-11-3/jquery-min.js')}" />
	<apex:includeScript value="{!URLFOR($Resource.Vendor, 'ember/2.0.0/ember.min.js')}" />

	<!-- Application -->
	<apex:includeScript value="{!URLFOR($Resource.SampleApp)}" />

	<!-- Container -->
	<div id="ember-app"></div>
	
	<!-- Get the session ID for AJAX requests to SalesForce -->
	<!-- This script tag must be added at the end of the page -->
	<script>
		var slc = slc || {};
		slc.API.sessionId = '{!$Api.Session_ID}';
	</script>
</apex:page>
```

### Communicating with the SalesForce API

In order to make an Ember application useful for SalesForce, you'll want to access/modify records in the database and possibly execute Apex code. [slc-ember](https://git.starlightconsultants.com/starlight/slc-ember.git) provides an utility to query the API directly. The session ID is required to make AJAX calls and the easiest way to get it is to use VisualForce merge variables. When the page is processed, `{!$Api.Session_ID}` will be populated before being served to the user.

### Executing Server-Side Code

It is possible to make use of JavaScript Remoting in SalesForce. However, the best way to execute server-side code is to write REST resources and call them. With [slc-ember](https://git.starlightconsultants.com/starlight/slc-ember.git) and the [Starlight Toolkit](https://git.starlightconsultants.com/starlight/starlight-toolkit.git) (installed as a package in SalesForce), you could query a resource like this:

Client-Side JavaScript: 
```javascript
define('route:notes', ['slc:sf'], function(sf) {
	return Ember.Route.extend({
		// model gets a note record from the database
		model: function() {
			return sf.apex(
				'GET',
				'/stk/NoteService.GetModel?note=1234'
			});
		}
	});
});
```

Server-Side Apex:
```java
global class NoteService {
	
	// GetModel returns a Note record
	global class GetTeamMembers extends stk.Endpoint {
		global override void runGet(RestRequest req, stk.ServiceResponse res) {
			if(!req.params.containsKey('note'))
				throw new stk.ServiceException(400, 'No note ID specified');
			
			Note note = [
				SELECT Id, Subject, Description
				FROM Note
				WHERE Id = :req.params.get('note')
				LIMIT 1
			];
			
			res.status(200).json(note);
		}
	}
}
```