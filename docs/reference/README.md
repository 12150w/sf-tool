SalesForce Tool API Reference
========================================

The API reference documents the modules and classes available as a public API.


### Running the SF Tool

The sf tool is run from the command line using node to execute `sf.js` in the root folder.