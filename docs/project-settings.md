# Project Settings
Each project contains a settings file named `sf-project.yaml`.
The contents of this file are described in the following tree.

* **[connections]** `Object`: A listing of connection data where the key is the
	connection name
	* **refresh_token** `String`: The refresh token for the connection
	* **instance_url** `String`: The resource server instance URL
	* **id** `String`: The identifier received during authentication
	* **login_server** `String`: The login server to authenticate the connection
	* **urls** `Object`: The urls associated with this connection
	* **info** `Object`: Contains various information about the connection such as
		whether there is a managed package at this connection or not
		* **namespace** `String`: The managed package namespace or `null`
* **[package]** `Object`: A listing of all project components
	*(this maps the metadata type name to a list of synced component full names)*

:information_source: Properties surrounded in square brackets indicate optional
	properties



## Project Cache
In addition the the project settings each project contains a cache stored in the
	`.sf-cache.yaml` file.
The contents of the cache are described as follows.

* **[sessions]** `Object`: The latest known session id for each connection (the
	keys match to the connection name in the project settings)

:warning: The file `.sf-cache.yaml` should be added to the project git ignore
	file
