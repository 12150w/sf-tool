SalesForce Tool Documentation
======================================================================



Actions
----------------------------------------------------------------------
Actions are run from the command line:

```
sf <action name>
```

Actions can have 0 or more arguments and/or options:

```
sf <action name> <arg1> <arg2> ... <argN> --<option1>=<option1 value>
```

To list all the available actions run:

```
sf help
```

To list the help information for an action run:

```
sf help <action name>
```

Project Settings
----------------------------------------------------------------------
The project settings contains OAuth connection details and lists of metadata components by type. Further details can be found [here](docs/project-settings.md).


Ember Applications
----------------------------------------------------------------------

Ember applications can can be built into a static resource that is usable on VisualForce pages. Further details for building Ember applications in SalesForce can be found [here](docs/ember.md)



Metadata
----------------------------------------------------------------------

There are 4 types of metadata that all SalesForce metadata types extend. Further details can be found [here](docs/metadata.md).

| Name | Description |
|------|-------------|
| Metadata | Standard metadata. Base class for all metadata |
| Content Metadata | Metadata with an associated source code file |
| Compound Metadata | Metadata that contains sub-metadata |
| Sub-Metadata | Metadata that further defines a containing compound metadata |