SalesForce Development Tool
======================================================================

This is a command line development tool that allows for the editing and saving of SalesForce source code or other metadata types such as objects, layouts and fields. This tool also allows Ember Applications to easily be built into VisualForce pages. It works under Windows, OS X, or Linux.


## Installation
These are one time instructions for installing the tool.

1. Install [Node.js](http://nodejs.org/)
2. Run the following command

```
npm install -g sf-tool
```


## Updating
Updating is simple just run the following command.

```
npm update -g sf-tool
```


## Documentation
All documentation is stored in the [docs folder](https://gitlab.com/12150w/sf-tool/blob/master/docs/README.md). The API reference can be found in the [reference folder](https://gitlab.com/12150w/sf-tool/tree/master/docs/reference).


## Example Usage

### Create a SalesForce Tool Project

```
cd project_directory
sf init
```

Then sign in with your salesforce credentials. A refresh token is stored in the project settings so sign in is not required every time.

### Access Command Help

```
sf help
sf help add-connection
```

### Create New ApexClass

```
cd project_directory
sf create ApexClass ApexClassName
```

### Retrieve Existing ApexClass From SalesForce for Editing

```
cd project_directory
sf add ApexClass ApexClassName
```

### Save ApexClass Changes to SalesForce

```
cd project_directory
sf save ApexClass:ApexClassName
```

### Delete ApexClass From Project and SalesForce

```
cd project_directory
sf delete ApexClass ApexClassName
```

### Delete ApexClass From Project

```
cd project_directory
sf delete ApexClass ApexClassName --localOnly
```

### Deploy to Production Instance

Add the production environment connection:

```
cd project_directory
sf add-connection production --server=login.salesforce.com
```

Sign in with credentials, then deploy the changes

```
sf save ApexClass:ApexClassName --conn=production
```