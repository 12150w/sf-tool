FROM node:8-alpine
RUN npm install -g sf-tool --unsafe-perm=true --allow-root
CMD ["/bin/bash"]
