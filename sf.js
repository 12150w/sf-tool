#!/usr/bin/env node
var q = require('q'),
	errors = require('./src/base/errors'),
	ui = require('./src/base/ui'),
	actionRegistry = require('./src/cli/action-registry'),
	parseArguments = require('minimist');

/* 
 * CLI Interface
 * 
 */
ui.enablePrint();
q( parseArguments(process.argv.slice(2)) ).then(function(cliArgs) {
	// Organize the arguments
	if(cliArgs._.length < 1) throw new errors.UIError('No action specified');
	
	var actionKey = cliArgs._.splice(0, 1)[0];
	var action = actionRegistry.get(actionKey.toLowerCase());
	
	var actionArgs = cliArgs._;
	delete cliArgs._;
	
	return action._runProxy(actionArgs, cliArgs || {});
	
}).fail(function(err) {
	ui.printError(err);
	process.exit(-1);
}).done();
