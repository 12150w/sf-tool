/*
	Electron Runner
		Starts up and runs an electron application
*/
var path = require('path'),
	q = require('q'),
	electronPath = require('electron'),
	ElectronError = require('../base/errors').ElectronError,
	spawn = require('child_process').spawn;

module.exports = {
	
	// runs the electron application in the folder /electron/apps
	run: function(appPath, data) {
		data = data || {};
		
		// Construct the full app path
		appPath = appPath.replace(/\//g, path.sep);
		var fullPath = path.join(__dirname, 'apps', appPath);
		
		// Start electron
		var appDefer = q.defer(),
			appChild = spawn(electronPath, [fullPath], {
				detached: true,
				stdio: [
					'ignore', // stdin
					'pipe',   // stdout
					'pipe',   // stderr
					'ipc'
				]
			});
		
		// Handle incoming data
		var messageBuffer = '',
			endMessage = undefined;
		appChild.on('message', function(message) {
			if(message.type === 'update') {
				appDefer.notify(message.data);
			} else if(message.type === 'end') {
				endMessage = message.data;
			}
		});
		
		// Handle error and status messaging
		var appOutput = '';
		appChild.stdout.on('data', function(chunk) {
			appOutput += chunk.toString('utf8');
		});
		appChild.stderr.on('data', function(chunk) {
			appOutput += chunk.toString('utf8');
		});
		
		// Handle application exit & error
		appChild.on('exit', function(code, signal) {
			if(code !== 0 && code !== null && endMessage === undefined) {
				appDefer.reject(new ElectronError(
					'Electron failed with status code ' + code,
					appOutput
				));
			} else {
				appDefer.resolve(endMessage);
			}
		});
		appChild.on('error', function(err) {
			appDefer.reject(err);
		});
		
		// Send the start data
		appChild.send(data);
		
		return appDefer.promise;
	}
	
};