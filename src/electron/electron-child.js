var electron = {},
	q = require('q');

electron.emit = function(data, isEnd) {
	var msgType = isEnd === true ? 'end' : 'update';
	
	process.send({
		type: msgType,
		data: data
	});
};


var startMsg = undefined,
	msgWaiters = [];
process.on('message', function(msg) {
	startMsg = msg;
	
	msgWaiters.forEach(function(waiter) {
		waiter(msg);
	});
});


electron.getMessage = function() {
	if(startMsg !== undefined) return q(startMsg);
	
	var defer = q.defer();
	msgWaiters.push(function(msg) {
		defer.resolve(msg);
	});
	
	return defer.promise;
};


module.exports = electron;
