/* Authorize Applications
 *    Allows the user to log in to SalesForce
 *    Returns the oauth information
 */
var electron = require('../electron-child'),
	ef = require('electron'),
	app = ef.app,
	oauth = require('../../base/oauth'),
	BrowserWindow = ef.BrowserWindow;

var REDIRECT_URI = 'sf-tool://callback';


app.on('ready', function() {
	var win = null,
		options = null;
	
	// Load the startup data
	electron.getMessage().then(function(msg) {
		// Construct the options
		options = msg;
		options.loginServer = options.loginServer || 'test.salesforce.com';
		
		if(options.limitedAccess === true) {
			options.oauthClientId = oauth.RESTRICTED_CLIENT_ID;
		} else {
			options.oauthClientId = oauth.CLIENT_ID;
		}
		
	}).then(function() {
		// Construct the request URL
		var url = 'https://' + options.loginServer;
		url += '/services/oauth2/authorize?';
		url += 'response_type=token';
		url += '&client_id=' + encodeURIComponent(options.oauthClientId);
		url += '&redirect_uri=' + encodeURIComponent(REDIRECT_URI);
		url += '&display=popup';
		url += '&prompt=consent';
		return url;
		
	}).then(function(requestUrl) {
		// Open the window
		win = new BrowserWindow({width: 800, height: 600});
		
		win.on('page-title-updated', function(event) {
			event.preventDefault();
		});
		win.setTitle('SalesForce: ' + options.loginServer);
		win.loadURL(requestUrl);
		
	}).then(function() {
		// Handle the will navigate event
		win.webContents.on('will-navigate', function(event, url) {
			
			if(url.indexOf(REDIRECT_URI) !== -1) {
				
				// Check for error
				var errorMatch = /error=([^&]+)/.exec(url);
				if(errorMatch != null) {
					var errorInfo = decodeURIComponent(errorMatch[1]).replace(/\+/g, ' ' );
					
					// Check for more details
					var descriptionMatch = /error_description=([^&]+)/.exec(url);
					if(descriptionMatch != null) {
						errorInfo = decodeURIComponent(descriptionMatch[1]).replace(/\+/g, ' ' );
					}
					
					electron.emit({
						error: errorInfo
					}, true);
					app.quit();
					return;
				}
				
				// Parse the url
				var argBits = /#(.+)/.exec(url)[1],
					bits = argBits.split('&'),
					oauthData = {};
				
				// Parse the data
				var parts;
				for(var i=0; i<bits.length; i++) {
					parts = bits[i].split('=');
					oauthData[parts[0]] = unescape(parts[1]);
				}
				
				electron.emit({
					error: null,
					
					access_token: oauthData.access_token,
					refresh_token: oauthData.refresh_token,
					instance_url: oauthData.instance_url,
					id: oauthData.id,
					scope: oauthData.scope.split('+'),
					token_type: oauthData.token_type,
					login_server: options.loginServer
				}, true);
				app.exit(0);
			}
		});
		
	}).done();
	
});