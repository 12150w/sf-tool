/**
	@class ConvertAction
	@classdesc Converts an old sf-tool project to the new format
	@extends Action
*/
var Action = require('./types/action'),
	Project = require('../../base/project'),
	UIError = require('../../base/errors').UIError,
	Connection = require('../../connection/connection'),
	InitAction = require('./init'),
	yaml = require('js-yaml'),
	q = require('q'),
	path = require('path'),
	fs = require('fs-extra');

module.exports = Action.extend({
	key: 'convert',
	description: 'Converts an old sf-tool project to the new format',
	examples: {
		'sf convert': 'Converts the project in the current directory to the new format'
	},
	
	/** Converts a project to the latest version
		@memberof ConvertAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@return {Promise}
	*/
	run: function(args, opts) {
		var proj = new Project();
		
		return this._convertMainConnection(proj).then(this._convertPackage);
	},
	
	/** Converts the main server to the new format
		@memberof ConvertAction
		@private
	*/
	_convertMainConnection: function(proj) {
		var settingsPath = path.join(proj.path(), 'sf-settings.yaml'),
			authPath = path.join(proj.path(), '.sf-auth.yaml'),
			loginServer = null;
		
		// Read the settings file
		return q.nfcall(fs.readFile, settingsPath, {
			encoding: 'utf8'
			
		}).then(function(rawSettings) {
			loginServer = yaml.safeLoad(rawSettings).loginServer;
			return q.nfcall(fs.unlink, settingsPath);
			
		}).fail(function(err) {
			// Handle no settings
			if(err.code !== 'ENOENT') throw err;
			throw new UIError('sf-settings.yaml was not found, is this a valid old sf-tool project?');
			
		}).then(function() {
			// Delete .sf-auth.yaml
			return q.nfcall(fs.unlink, authPath);
			
		}).fail(function(err) {
			// Handle no auth file
			if(err.code !== 'ENOENT') throw err;
			throw new UIError('.sf-auth.yaml was not found, is this a valid old sf-tool project?');
			
		}).then(function() {
			// Initialize the new project
			return InitAction.prototype.run([], {
				server: loginServer
			});
			
		}).then(function() {
			return proj;
			
		});
	},
	
	/** Converts the package to the new format
		@memberof ConvertAction
		@private
	*/
	_convertPackage: function(proj) {
		var packagePath = path.join(proj.path(), 'package.yaml'),
			componentPath = path.join(proj.path(), 'metadata');
		
		// Read package.yaml
		return q.nfcall(fs.readFile, packagePath, {
			encoding: 'utf8'
			
		}).then(function(rawPackage) {
			// Delete the package.yaml
			return q.nfcall(fs.unlink, packagePath).then(function() {
				return rawPackage;
			});
			
		}).fail(function(err) {
			// Handle no package.yaml
			if(err.code !== 'ENOENT') throw err;
			
		}).then(function(rawPackage) {
			// Convert the package
			if(rawPackage == null) {
				return;
			}
			var packData = yaml.safeLoad(rawPackage);
			
			return proj.settings.set('package', packData);
			
		}).then(function() {
			// Rename the component folder if there is one
			var newComponentPath = path.join(proj.path(), 'sf');
			return q.nfcall(fs.rename, componentPath, newComponentPath).fail(function(err) {
				if(err.code !== 'ENOENT') throw err;
			});
			
		}).then(function() {
			return proj;
		});
	}
	
});