/**
	@class ToolAction
	@classdesc Saves tooling supported objects via the Tooling API.
				Use this when Salesforce is being slow (which is pretty common).
	@extends ConnectionAction
*/
var ConnectionAction = require('./types/connection-action'),
	Package = require('../../base/package'),
	ui = require('../../base/ui'),
	chalk = require('chalk'),
	q = require('q'),
	crypto = require('crypto'),
	UIError = require('../../base/errors').UIError
	HTTPError = require('../../base/errors').HTTPError;

// metadata types supported by the Tooling API (and this action)
var _supportedTypes = ['ApexClass', 'ApexTrigger', 'ApexPage', 'ApexComponent'];

// metadata types that are static resources
var _staticResourceTypes = ['SFZipFile', 'SFEmberApp', 'StaticResource'];

module.exports = ConnectionAction.extend({
	key: 'tool',
	description: 'Saves supported metadata with the Tooling API',
	examples: {
		'sf tool some/set.yaml': 'Saves supported metadata in some/set.yaml',
		'sf tool ApexClass:SomeClass': 'Save the Apex Class named SomeClass'
	},
	
	/** Saves metadata components to the connection server using the Tooling API
		@memberof ToolAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var pack = new Package(conn, false),
			containerHash = crypto.createHash('sha1'),
			saveComponents = [],
			containerId = null;
		
		containerHash.update('sf-tool', 'utf8');
		
		return pack.addArgs(args).then(function() {
			// Load only supported components
			pack.getComponents().forEach(function(component) {
				if(_supportedTypes.indexOf(component.typeName) < 0 && _staticResourceTypes.indexOf(component.typeName) < 0) {
					ui.print(chalk.yellow('Unsupported Metadata ' + component.typeName + ' ' + component.fullName + ' will not be saved'));
					return;
				}
				else {
					saveComponents.push(component);
					containerHash.update(component.typeName + ':' + component.fullName, 'utf8');
				}
			});
			
			if(saveComponents.length < 1) throw new UIError('No supported components to save');
			
		}).then(function() {
			// Set up the metadata container
			ui.print(chalk.gray('Loading Metadata Container'));
			var containerName = 'sf:' + containerHash.digest('base64');
			
			return conn.sfRequest('/services/data/v38.0/tooling/query', 'GET', {
				params: {
					q: 'SELECT Id FROM MetadataContainer WHERE Name = \'' + containerName + '\' LIMIT 1'
				}
			}).then(function(searchRes) {
				if(searchRes.json().size > 0) return searchRes.json().records[0].Id;
				
				return conn.sfRequest('/services/data/v38.0/tooling/sobjects/MetadataContainer', 'POST', {
					body: {
						Name: containerName
					}
				}).then(function(createRes) {
					return createRes.json().id;
				});
				
			});
			
		}).then(function(setupContainerId) {
			// Add the component members
			containerId = setupContainerId;
			ui.print(chalk.gray('Creating Component Members'));
			
			var memberPromises = saveComponents.map(function(meta) {
				return meta.read().then(function(data) {
					
					// Check for static resource.
					if(_staticResourceTypes.indexOf(meta.typeName) > -1) {
						return conn.sfRequest('/services/data/v38.0/query', 'GET', {
							params: {q: 'SELECT Id FROM StaticResource WHERE Name = \'' + meta.fullName + '\' LIMIT 1'}
						}).then(function(idRes) {
							if(idRes.json().size < 1) throw new UIError('Unable to locate ' + meta.typeName + ' with name ' + meta.fullName);
							
							return idRes.json().records[0].Id;
						}).then(function(resourceId) {
							return conn.sfRequest('/services/data/v38.0/tooling/sobjects/StaticResource/' + resourceId, 'PATCH', {
								body: {
									Body: data.content.toString('base64')
								}
							});
						});
					}
					
					var memberTypeName = meta.typeName + 'Member',
						metaId = null;
					
					// Load the class Id
					return conn.sfRequest('/services/data/v38.0/query', 'GET', {
						params: {q: 'SELECT Id FROM ' + meta.typeName + ' WHERE Name = \'' + meta.fullName + '\' LIMIT 1'}
					
					}).then(function(idRes) {
						if(idRes.json().size < 1) throw new UIError('Unable to locate ' + meta.typeName + ' with name ' + meta.fullName);
						metaId = idRes.json().records[0].Id;
						if(!data.content) throw new UIError('Unable to load content for ' + meta.typeName + ' with name ' + meta.fullName);
						
						return conn.sfRequest('/services/data/v38.0/tooling/sobjects/' + memberTypeName, 'POST', {
							body: {
								MetadataContainerId: containerId,
								ContentEntityId: metaId,
								Body: data.content.toString('utf8'),
								Metadata: data.metadata
							}
						});
						
					}).fail(function(err) {
						// check for re-use error
						if(err instanceof HTTPError && err.res.statusCode === 400) {
							var body = err.res.json();
							
							if(Object.prototype.toString.call(body) === '[object Array]' && body[0].errorCode === 'DUPLICATE_VALUE') {
								// Update the existing member
								ui.print(chalk.gray('Re-using existing member for ' + meta.typeName + ' '+ meta.fullName), 1);
								return conn.sfRequest('/services/data/v38.0/tooling/query', 'GET', {
									params: {
										q: 'SELECT Id FROM ' + memberTypeName + ' WHERE ContentEntityId = \'' + metaId + '\' AND MetadataContainerId = \'' + containerId + '\' LIMIT 1'
									}
								}).then(function(queryRes) {
									if(queryRes.json().totalSize < 1) throw new UIError('Unable to re-use member for ' + memberTypeName + ' ' + meta.fullName);
									
									return conn.sfRequest('/services/data/v38.0/tooling/sobjects/' + memberTypeName + '/' + queryRes.json().records[0].Id, 'PATCH', {
										body: {Body: data.content.toString('utf8')}
									});
								});
							}
						}
						
						throw err;
					});
				});
			});
			
			return q.all(memberPromises);
			
		}).then(function() {
			// Save the container
			ui.print(chalk.gray('Creating Save Request'));
			
			return conn.sfRequest('/services/data/v38.0/tooling/sobjects/ContainerAsyncRequest', 'POST', {
				body: {
					IsCheckOnly: false,
					MetadataContainerId: containerId
				}
			});
			
		}).then(function(asyncCreateRes) {
			// Wait for the save to complete
			var reqId = asyncCreateRes.json().id;
			
			var loadReqStatus = function() {
				var waitDefer = q.defer();
				setTimeout(function() {
					waitDefer.resolve();
				}, 750);
				
				return waitDefer.promise.then(function() {
					return conn.sfRequest('/services/data/v38.0/tooling/sobjects/ContainerAsyncRequest/' + reqId, 'GET');
					
				}).then(function(reqRes) {
					if(reqRes.json().State === 'Queued') {
						return loadReqStatus();
					} else {
						return reqRes.json();
					}
				});
			};
			
			return loadReqStatus();
			
		}).then(function(finalRes) {
			if(finalRes.State === 'Completed') {
				ui.print(chalk.green.bold('Save Completed'));
			} else {
				ui.print(chalk.red.bold('Save ' + finalRes.State));
			}
			
			finalRes.DeployDetails.allComponentMessages.forEach(function(metaRes) {
				if(metaRes.success === true) {
					ui.print(chalk.green(metaRes.componentType + ' ' + metaRes.fullName + ': Success'), 1);
				} else {
					ui.print(chalk.red(metaRes.componentType + ' ' + metaRes.fullName + ': Failed'), 1);
					ui.print(chalk.gray(metaRes.problem + ' (Line ' + metaRes.lineNumber + ')'), 2);
				}
			});
			
		});
	}
	
});