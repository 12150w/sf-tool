/**
	@class UploadAction
	@classdesc Uploads metadata components to a connection
	@extends ConnectionAction
*/
var ConnectionAction = require('./types/connection-action'),
	Package = require('../../base/package'),
	UIError = require('../../base/errors').UIError,
	fs = require('fs'),
	readline = require('readline'),
	os = require('os'),
	q = require('q'),
	chalk = require('chalk'),
	path = require('path'),
	ui = require('../../base/ui');

module.exports = ConnectionAction.extend({
	key: 'upload',
	description: 'Uploads a component zip file to a SalesForce connection',
	examples: {
		'sf upload path-to-zip-file': 'Saves the zip file package to SalesForce'
	},
	options: {
		'-v': 'Sets the deployment to verify only, no changes will be saved',
		'--test': 'Add a test class to be run with this deployment',
		'--min': 'Include this flag to minify SFEmberApp components being saved',
	},
	
	/** Uploads a zip file containing metadata to a SalesForce connection
		@memberof ExportAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var pack = new Package(conn, opts.min),
			self = this;
		
		if(args.length < 1) {
			throw new UIError('No filepath specified');
		}
		
		return q.ninvoke(fs, 'readFile', args[0]).then(function(zip) {
			
			return pack.send(zip, opts).progress(function(statusMsg) {
				// Print progress
				if(statusMsg == null) return;
				
				readline.clearLine(process.stdout, 0);
				readline.cursorTo(process.stdout, 0);
				process.stdout.write(chalk.gray(statusMsg));
				
			}).then(function(res) {
				// Go to new line
				process.stdout.write(os.EOL);
				return res;
				
			});
			
		}).then(function(deployRes) {
			// Display information about the deployment
			ui.printDeployRes(deployRes.res);
			self.showDebug(deployRes.headers);
			
			if(deployRes.res.success != true) {
				throw new UIError('Deployment failed');
			}
			
		});
	}
	
});