/**
	@class OpenAction
	@classdesc Opens the SalesForce web interface for a Connection
	@extends ConnectionAction
*/
var ConnectionAction = require('./types/connection-action'),
	ui = require('../../base/ui');

module.exports = ConnectionAction.extend({
	key: 'open',
	description: 'Opens the SalesForce web interface',
	examples: {
		'sf open': 'Opens the default connection',
		'sf open --conn=production': 'Opens the connection named "production"'
	},
	
	/** Opens a browser window and logs in to the SalesForce connection instance
		@memberof OpenAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var data;
		
		return conn.loadConfig().then(function(readConfig) {
			// Load the authorization identity
			data = readConfig;
			return conn.sfRequest(data.id, 'GET').then(function(res) {
				return conn.loadConfig().then(function(updatedConfig) {
					data = updatedConfig;
					return res;
				});
			});
			
		}).then(function(res) {
			// Open salesforce
			ui.print('Logging in as ' + res.json().display_name);
			
			var landingUrl = data.instance_url + '/secur/frontdoor.jsp';
			landingUrl += '?sid=' + encodeURIComponent(data.access_token);
			
			ui.openBrowser(landingUrl);
			
		});
	}

});