/**
	@class SyncAction
	@classdesc Downloads components from salesforce
	@extends ConnectionAction
*/
var ConnectionAction = require('./types/connection-action'),
	Package = require('../../base/package'),
	ui = require('../../base/ui'),
	chalk = require('chalk'),
	q = require('q');

module.exports = ConnectionAction.extend({
	key: 'sync',
	description: 'Downloads components from Salesforce',
	examples: {
		'sf sync': 'Downloads the entire package',
		'sf sync sets/example.yaml': 'Downloads the components in example.yaml',
		'sf sync ApexClass:SomeClass': 'Downloads SomeClass'
	},
	
	/** Downloads metadata components from the connection server
		@memberof SyncAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var pack = new Package(conn);
		
		return q.fcall(function() {
			// Verify if full package download
			if(args == null || args.length < 1) {
				return ui.verify('This will overwrite all local components, continue?');
			}
			
		}).then(function() {
			// Add the components
			return pack.addArgs(args);
			
		}).then(function() {
			// Sync the package
			ui.print(chalk.gray('Downloading components'));
			return pack.sync();
			
		}).then(function(syncedMeta) {
			
			// Print synced components
			ui.print(chalk.bold.green('Sync Successful'));
			syncedMeta.forEach(function(meta) {
				ui.print(chalk.gray(meta.typeName + ' ' + meta.getFullName()), 1);
			});
			
		});
	}
	
});
