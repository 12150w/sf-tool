/**
	@class AddAction
	@classdesc Adds a component to the project then adds it
	@extends ConnectionAction
*/
var q = require('q'),
	ui = require('../../base/ui'),
	typesRegistry = require('../../metadata/types/registry'),
	chalk = require('chalk'),
	ConnectionAction = require('./types/connection-action'),
	Package = require('../../base/package'),
	UIError = require('../../base/errors').UIError;

module.exports = ConnectionAction.extend({
	
	key: 'add',
	description: 'Adds an existing metadata component to the project',
	examples: {
		'sf add ApexClass SomeClass': 'Adds the class named "SomeClass"',
		'sf add CustomObject Name__c': 'Adds the object named "Name__c"'
	},
	
	/** Adds metadata component to the project
		@memberof AddAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var component, MetadataType;
		
		return q.fcall(function() {
			// Get the type name
			if(args.length < 1) {
				return ui.prompt('Metadata Type Name', {
					required: true,
					completer: typesRegistry.completer
				});
			} else {
				return args[0];
			}
			
		}).then(function(name) {
			// Lookup the metadata type
			return typesRegistry.forName(name);
			
		}).then(function(type) {
			// Get the component name
			MetadataType = type;
			if(args.length < 2) {
				return ui.prompt('ComponentName', {
					required: true,
					completer: typesRegistry.typeCompleter(MetadataType, conn)
				});
			} else {
				return args[1];
			}
			
		}).then(function(componentName) {
			// Check for component in package
			component = new MetadataType(componentName, conn.project);
			
			return conn.project.containsComponent(component).then(function(exists) {
				if(exists) {
					var typeName = component.typeNameAlias != null ? component.typeNameAlias : component.typeName;
					throw new UIError(typeName + ' ' + component.fullName + ' has already been added to this project');
				}
			});
			
		}).then(function() {
			// Download the metadata
			var pack = new Package(conn),
				typeName = component.typeNameAlias != null ? component.typeNameAlias : component.typeName;
			pack.addComponent(component);
			
			ui.print('Downloading ' + typeName + ' ' + component.fullName);
			return pack.sync();
			
		}).then(function() {
			// Save the component in the package
			return conn.project.addComponent(component);
			
		}).then(function() {
			// Finish up
			ui.print(chalk.bold.green('Successfully added component'));
			
		});
	}
	
});
