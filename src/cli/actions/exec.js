/**
	@class ExecAction
	@classdesc Runs an execute anonymous script.
	@extends ConnectionAction
*/
var ConnectionAction = require('./types/connection-action'),
	UIError = require('../../base/errors').UIError,
	ui = require('../../base/ui'),
	chalk = require('chalk'),
	fs = require('fs');

module.exports = ConnectionAction.extend({
	key: 'exec',
	description: 'Runs an execute anonymous script',
	examples: {
		'sf exec script.apex': 'Runs script.apex via execute anonymous'
	},
	
	/** Deploys metadata components from one connection to another (usually a sandbox to a production)
		@memberof ExecAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var scriptPath;
		var self = this;
		
		return q.fcall(function() {
			// Verify the file name
			if(args.length < 1) {
				throw new UIError('No script file specified');
			}
			scriptPath = args[0];
			
			return q.nfcall(fs.readFile, scriptPath, {encoding: 'utf8'}).fail(function(err) {
				if(err.code === 'ENOENT') {
					throw new UIError('Unable to open script ' + scriptPath);
				} else {
					throw err;
				}
			});
			
		}).then(function(code) {
			// Run the code
			ui.print('Running ' + scriptPath);
			return conn.apex.run('executeAnonymous', {
				apexcode: code
			});
			
		}).then(function(data) {
			if(data.res.success !== true) {
				if(data.res.compiled != true) {
					throw new UIError('Unable to compile: ' + data.res.compileProblem + ' (line ' + data.res.line + ')');
				} else {
					throw new UIError(data.res.exceptionMessage + ' (line ' + data.res.line + ')');
				}
			}
			self.showDebug(data.headers);
			
		});
	}
	
});
