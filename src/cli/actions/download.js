/**
	@class DownloadAction
	@classdesc Downloads metadata components to the hard disk
	@extends ConnectionAction
*/
var ConnectionAction = require('./types/connection-action'),
	Package = require('../../base/package'),
	fs = require('fs'),
	q = require('q'),
	chalk = require('chalk'),
	path = require('path'),
	ui = require('../../base/ui');

module.exports = ConnectionAction.extend({
	key: 'download',
	description: 'Downloads components as a Salesforce package zip file',
	examples: {
		'sf download --out=package.zip': 'Exports all components in the project to package.zip',
		'sf download --out=partial.zip some-set.yaml': 'Downloads components in some-set.yaml to partial.zip'
	},
	options: {
		'--out': 'The path to the output zip file to create (defaults to "download.zip")',
		'--min': 'Include this flag to minify SFEmberApp components being exported'
	},
	
	/** Downloads a zip file containing metadata to the hard disk
		@memberof ExportAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var pack = new Package(conn, opts.min),
			outPath = opts.out || path.join(process.cwd(),  'download.zip');
		
		return pack.addArgs(args).then(function() {
			return pack.retrieve();
		}).then(function(response) {
			return q.nfcall(fs.writeFile, outPath, new Buffer(response.res.zipFile, 'base64'));
		}).then(function() {
			
			return ui.print(chalk.green('Successfully downloaded to "' + outPath + '"'));
			
		});
	}
	
});