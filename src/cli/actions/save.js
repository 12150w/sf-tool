/**
	@class SaveAction
	@classdesc Commits a package to salesforce
	@extends ConnectionAction
*/
var ConnectionAction = require('./types/connection-action'),
	UIError = require('../../base/errors').UIError,
	Package = require('../../base/package'),
	q = require('q'),
	readline = require('readline'),
	yaml = require('js-yaml'),
	chalk = require('chalk'),
	os = require('os'),
	ui = require('../../base/ui');

module.exports = ConnectionAction.extend({
	key: 'save',
	description: 'Saves components to SalesForce',
	examples: {
		'sf save': 'Saves all project components',
		'sf save sets/some-set.yaml': 'Saves the components defined in some-set.yaml',
		'sf save --conn=production': 'Saves all components using the production connection',
		'sf save ApexClass:DemoClass': 'Saves the ApexClass named DemoClass',
		'sf save sets/set.yaml --test TestClass1 --test TestClass2': 'Saves the set and runs TestClass1 and TestClass2',
		'sf save set.yaml --deleteAfter ApexClass:SomeClass --deleteAfter delete.yaml': 'Saves set.yaml then deletes SomeClass and components in delete.yaml'
	},
	options: {
		'-v': 'Sets the deployment to verify only, no changes will be saved',
		'--test': 'Add a test class to be run with this deployment',
		'--min': 'Include this flag to minify SFEmberApp components being saved',
		'--deleteBefore': 'Adds components, or component sets, to be deleted before the package saves',
		'--deleteAfter': 'Adds components, or component sets, to be deleted after the package saves'
	},
	
	/** Saves metadata components to the connection server
		@memberof SaveAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var self = this,
			pack = new Package(conn, opts.min);
		
		// Verify if saving the entire package
		return q.fcall(function() {
			if(args == null || args.length < 1) {
				return ui.verify(
					'This will overwrite all components on SalesForce, continue?'
				);
			}
			
		}).then(function() {
			// Add components from arguments
			return pack.addArgs(args, {
				deleteBefore: opts.deleteBefore,
				deleteAfter: opts.deleteAfter
			});
			
		}).then(function() {
			// Save the package
			if(opts.v === true) {
				ui.print(chalk.gray('Verify Only: components will not be saved'));
			}
			ui.print(chalk.gray('Starting Save'));
			
			return pack.save({
				rollback: true,
				verifyOnly: opts.v,
				tests: opts.test
				
			}).progress(function(statusMsg) {
				// Print progress
				if(statusMsg == null) return;
				
				readline.clearLine(process.stdout, 0);
				readline.cursorTo(process.stdout, 0);
				process.stdout.write(chalk.gray(statusMsg));
				
			}).then(function(res) {
				// Go to new line
				process.stdout.write(os.EOL);
				return res;
				
			});
			
		}).then(function(deployRes) {
			// Display information about the deployment
			ui.printDeployRes(deployRes.res);
			self.showDebug(deployRes.headers);
			
			if(deployRes.res.success != true) {
				throw new UIError('Deployment failed');
			}
			
		});
		
	}
	
});
