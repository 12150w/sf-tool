/**
	@class TestAction
	@classdesc Runs Apex test classes
	@extends ConnectionAction
*/
var ConnectionAction = require('./types/connection-action'),
	UIError = require('../../base/errors').UIError,
	ui = require('../../base/ui'),
	chalk = require('chalk'),
	q = require('q');

// percentOf returns the percent value (as a number) of the numerator and denominator.
function percentOf(num, den) {
	if(den === 0) return 0;
	
	return Math.floor( (num/den)*100 );
};

module.exports = ConnectionAction.extend({
	key: 'test',
	description: 'Runs Apex unit tests',
	examples: {
		'sf test TestClass': 'Runs the unit test TestClass',
		'sf test TestClass --coverage': 'Runs the test class and returns coverage information',
		'sf test Test1 Test2': 'Runs multiple test classes',
		'sf test TestClass.TestMethod': 'Runs only the specified test method of a test class'
	},
	options: {
		'--coverage': 'Prints code coverage information from the tests',
		'--failures': 'Set the number of failed methods allowed (default -1)',
		'--all': 'Runs all the tests in the organization'
	},
	
	/** Runs Apex tests in the connection instance
		@memberof TestAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var self = this;
		args = args || [];
		
		opts.failures = opts.failures || '0';
		opts.failures = parseInt(opts.failures, 10);
		var runAll = null,
			connNamespace = null;
		
		return q.fcall(function() {
			// Verify some tests were specified
			if(opts.all != null) {
				runAll = true;
			} else if(args.length < 1) {
				throw new UIError('No test class was specified');
			}
			ui.print(chalk.gray('Running tests'));
			
		}).then(function() {
			// Load the connection namespace
			return conn.loadConfig().then(function(conf) {
				connNamespace = conf.info.namespace;
			});
			
		}).then(function() {
			// Group test run tokens by class
			var toRunMap = {};
			args.forEach(function(testToken) {
				var methodMatch = /^(\w+)\.(\w+)$/.exec(testToken);
				
				var className = testToken;
				if(methodMatch != null) {
					className = methodMatch[1];
				}
				
				if(toRunMap[className] === undefined) {
					toRunMap[className] = {
						all: methodMatch == null,
						methods: []
					};
				}
				
				if(methodMatch != null) {
					toRunMap[className].methods.push(methodMatch[2]);
				}
			});
			
			return toRunMap;
			
		}).then(function(toRunMap) {
			// Load the tests to run
			var testLoadPromises = [];
			
			Object.keys(toRunMap).forEach(function(className) {
				var runInfo = toRunMap[className];
				
				var loadPromise = conn.sfRequest('/services/data/v38.0/tooling/query', 'GET', {
					params: {
						q: 'SELECT Id, SymbolTable FROM ApexClass WHERE Name = \'' + className + '\' LIMIT 1'
					}
				}).then(function(queryRes) {
					var classRes = queryRes.json();
					if(classRes.records.length < 1) {
						throw new UIError('No class named ' + className + ' found');
					}
					var classInfo = classRes.records[0];
					
					var requestInfo = { classId: classInfo.Id };
					if(runInfo.all) {
						// Add all test methods
						requestInfo.testMethods = [];
						
						for(var i=0; i<classInfo.SymbolTable.methods.length; i++) {
							var methodInfo = classInfo.SymbolTable.methods[i];
							
							var isTestMethod = false;
							for(var j=0; j<methodInfo.annotations.length; j++) {
								if(methodInfo.annotations[j].name === 'IsTest') {
									isTestMethod = true;
									break;
								}
							}
							
							if(isTestMethod) {
								requestInfo.testMethods.push(methodInfo.name);
							}
						}
						
					} else {
						// Add test methods that were given
						requestInfo.testMethods = runInfo.methods;
						
					}
					
					return requestInfo;
				});
				
				testLoadPromises.push(loadPromise);
			});
			
			return q.all(testLoadPromises).then(function(runRequest) {
				//runRequest.push({ maxFailedTests: opts.failures });
				
				return runRequest;
			});
			
		}).then(function(runRequest) {
			// Run the tests
			var testBody = {
				tests: runRequest
			};
			
			return conn.sfRequest('/services/data/v38.0/tooling/runTestsSynchronous/', 'POST', {
				body: testBody
			});
			
		}).then(function(allInfo) {
			// Process the res
			var info = allInfo.json();
			if(info.numTestsRun == null || info.numTestsRun < 1) {
				return ui.print(chalk.bold.red('No tests were run, perhaps there is a typo'));
			}
			
			// Failures
			if(info.numFailures > 0) {
				ui.print(chalk.bold.red('Test Failures'));
				info.failures.forEach(function(failure) {
					ui.print(chalk.red(failure.name + '.' + failure.methodName), 1);
					ui.print(failure.message, 2);
					ui.print(chalk.gray(failure.stackTrace), 2);
				});
			}
			
			// Successes
			if((info.successes || []).length > 0) {
				ui.print(chalk.bold.green('Test Successes'));
				info.successes.forEach(function(success) {
					var msg = success.name + '.' + success.methodName + ' (' + Math.round(success.time) + ' ms)';
					ui.print(chalk.gray(msg), 1);
				});
			}
			
			// Coverage warnings
			if(info.codeCoverageWarnings != null) {
				info.codeCoverageWarnings.forEach(function(warn) {
					if(warn.message.indexOf('Test coverage of selected Apex Trigger is 0%') === 0) {
						return;
					}
					ui.print(chalk.yellow(warn.message));
				});
			}
			
			// Print coverage info if needed
			if(!!opts.coverage) {
				ui.print(chalk.bold('Coverage Details'));
				var coveredCount = 0,
					totalCount = 0;
				
				(info.codeCoverage || []).forEach(function(cov) {
					if(cov.namespace != null && cov.namespace != '') {
						if(cov.namespace != connNamespace) {
							return;
						}
					}
					
					coveredCount += cov.numLocations - cov.numLocationsNotCovered;
					totalCount += cov.numLocations;
					
					var covPercent = percentOf(cov.numLocations - cov.numLocationsNotCovered, cov.numLocations);
					if(covPercent === 0 && runAll !== true) {
						return;
					}
					
					var msg = cov.name + ': ' + covPercent + '% ';
					msg += '(' + (cov.numLocations - cov.numLocationsNotCovered) + '/' + cov.numLocations + ')';
					
					ui.print(chalk.gray(msg), 1);
				});
				
				var totalPercent = percentOf(coveredCount, totalCount);
				var totalFormatter = chalk.yellow;
				if(totalPercent >= 75) totalFormatter = chalk.green;
				
				var totalMsg = 'Total Coverage: ' + percentOf(coveredCount, totalCount) + '% (' + coveredCount + '/' + totalCount + ')';
				ui.print(totalFormatter(totalMsg));
			}
			
			// Print debug logs if needed
			return self.showToolingDebug(info.apexLogId).then(function() {
				if(info.numFailures > 0 && info.numFailures > opts.failures) {
					throw new UIError('Tests failed');
				}
			});
			
		});
	}
	
});
