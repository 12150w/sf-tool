/**
	@class InitAction
	@classdesc Creates a new sf project
	@extends Action
*/
var Action = require('./types/action'),
	UIError = require('../../base/errors').UIError,
	Project = require('../../base/project'),
	Connection = require('../../connection/connection'),
	path = require('path'),
	ui = require('../../base/ui'),
	electronRunner = require('../../electron/runner'),
	q = require('q'),
	fs = require('fs');

module.exports = Action.extend({
	key: 'init',
	description: 'Create a new project',
	options: {
		'--server': 'Login server (default test.salesforce.com)',
		'--limitedAccess': 'Use the limited access OAuath application which allows for connecting with limited privileges'
	},
	examples: {
		'sf init': 'Create a project in the current folder',
		'sf init <FOLDER>': 'Create a project in the folder <FOLDER>'
	},
	
	/** Initializes a new SF Tool project
		@memberof InitAction
		@instance
		@param args - Command line arguments
		@param options - Command line options
	*/
	run: function(args, options) {
		
		// Load the project path
		var projectPath = process.cwd();
		if(args.length > 0) {
			projectPath = args[0];
		}
		ui.print('Creating project ' + projectPath);
		
		return q.fcall(function() {
			// Check if this is an existing project
			return q.nfcall(fs.stat, path.join(projectPath, 'sf-project.yaml')).then(function() {
				throw new UIError('A project already exists at ' + projectPath);
			}, function(err) {
				if(err.code !== 'ENOENT') throw err;
			});
			
		}).then(function() {
			// Load the authorization data
			return electronRunner.run('authorize', {
				loginServer: options.server,
				limitedAccess: options.limitedAccess
			});
			
		}).then(function(oauth) {
			if(oauth == null) {
				throw new UIError('no authorization data');
			}
			if(oauth.error != null) {
				throw new UIError(oauth.error);
			}
			
			// Initialize the project and save data
			var newProj = new Project(projectPath);
			return newProj.initialize().then(function() {
				var defaultConn = new Connection(newProj);
				return defaultConn.storeAuthorization(oauth);
				
			});
			
		});
	}
	
});
