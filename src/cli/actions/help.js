/**
	@class Help Action
	@classdesc Displays action help
	@extends Action
*/
var Action = require('./types/action'),
	ui = require('../../base/ui'),
	q = require('q'),
	chalk = require('chalk'),
	UIError = require('../../base/errors').UIError;
	
module.exports = Action.extend({
	key: 'help',
	description: 'Displays help information',
	examples: {
		'sf help': 'View the general help',
		'sf help <ACTION>': 'View help for the specific action <ACTION>'
	},
	
	/** Prints help information
		@memberof HelpAction
		@instance
		@param {string[]} args - Command line arguments
		@return {Promise}
	*/
	run: function(args) {
		if(args.length < 1) {
			return this.printAllHelp();
		} else {
			return this.printActionHelp(args[0]);
		}
	},
	
	/** Prints the help information about an action to the user
		@memberof HelpAction
		@instance
		@param {string} actionName - The name of the action to print help for
		@return {Promise}
	*/
	printActionHelp: function(actionName) {
		// Require the registry here to avoid loading the module before it is fully defined
		var actionRegistry = require('../action-registry');
		
		var action = actionRegistry.get(actionName.toLowerCase());
		ui.print('');
		
		// General info
		ui.print(chalk.bold('Action: ' + action.key));
		ui.print(action.description, 1);
		
		// Options
		var allOptions = [];
		if(action.options != null) allOptions.push(action.options);
		if(action._sharedOpts.length > 0) {
			action._sharedOpts.forEach(function(optionSet) {
				allOptions.push(optionSet);
			});
		}
		if(allOptions.length > 0) {
			ui.print('');
			ui.print(chalk.underline('Options'));
			
			allOptions.forEach(function(optionSet) {
				for(var key in optionSet) {
					ui.print(chalk.bold(key + ': ') + optionSet[key], 1);
				}
			});
		}
		
		// Examples
		if(action.examples != null) {
			ui.print('');
			ui.print(chalk.underline('Examples'));
			
			for(var key in action.examples) {
				ui.print(chalk.bold(key), 1);
				ui.print(action.examples[key], 2);
			}
		}
		
		ui.print('');
	},
	
	/** Prints a list of actions
		@memberof HelpAction
		@instance
		@return {Promise}
	*/
	printAllHelp: function() {
		ui.print('');
		ui.print(chalk.underline('Available Actions'));
		
		// Require the registry here to avoid loading the module before it is fully defined
		var actionRegistry = require('../action-registry');
		
		var key,
			keys = actionRegistry.getKeys();
		for(var i=0; i<keys.length; i++) {
			var action = actionRegistry.get(keys[i]);
			ui.print(chalk.bold(action.key + ': ') + action.description, 1);
		}
		
		ui.print('');
		ui.print('To view action specific help run: ' + chalk.bold('sf help <ACTION>'));
		ui.print('');
		
		return q();
	}
	
});