/**
	@class AddConnectionAction
	@classdesc Allows creating new connections
	@extends ProjectAction
*/
var q = require('q'),
	ui = require('../../base/ui'),
	chalk = require('chalk'),
	electronRunner = require('../../electron/runner'),
	Project = require('../../base/project'),
	Connection = require('../../connection/connection'),
	UIError = require('../../base/errors').UIError,
	ProjectAction = require('./types/project-action');

module.exports = ProjectAction.extend({
	
	key: 'add-connection',
	description: 'Creates a new connection',
	options: {
		'--server': 'Login server (default test.salesforce.com)',
		'--limitedAccess': 'Use the limited access OAuath application which allows for connecting with limited privileges'
	},
	examples: {
		'sf add-connection production': 'Creates a new connection named "production"'
	},
	
	/** Adds a new connection to the project
		@memberof AddConnectionAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@return {Promise}
	*/
	run: function(args, opts) {
		var self = this;
		
		// Add the new connection
		return q().then(function() {
			// Get the new connection name
			if(args.length < 1 || !args[0]) {
				return ui.prompt('New Connection Name', {
					required: true
				});
			} else {
				return args[0];
			}
			
		}).then(function(newName) {
			// Check if this connection already exists
			var proj;
			return self.getProject().then(function(actionProj) {
				proj = actionProj;
				return proj.settings.get('connections.' + newName);
				
			}).then(function(existingAuth) {
				if(existingAuth != null) throw new UIError('A connection named "' + newName + '" already exists');
				
				return new Connection(proj, newName);
			});
			
		}).then(function(conn) {
			// Authorize the new connection
			ui.print('Requesting authorization for connection "' + conn.getName() + '"');
			return electronRunner.run('authorize', {
				loginServer: opts.server,
				limitedAccess: opts.limitedAccess
			}).then(function(oauth) {
				// Update the authorization
				if(oauth == null) {
					throw new UIError('no authorization data');
				}
				if(oauth.error != null) {
					throw new UIError(oauth.error);
				}
				
				return conn.storeAuthorization(oauth);
				
			}).then(function() {
				ui.print(chalk.bold(chalk.green('Successfully added connection "' + conn.getName() + '"')));
				
			});
			
		});
		
	}
	
});
