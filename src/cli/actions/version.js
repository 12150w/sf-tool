/**
	@class VersionAction
	@classdesc Prints the currently installed version of the tool
	@extends Action
 */
var Action = require('./types/action'),
	q = require('q'),
	fs = require('fs'),
	path = require('path'),
	chalk = require('chalk'),
	ui = require('../../base/ui');

module.exports = Action.extend({
	key:  'version',
	description: 'Check the current version of the sf-tool',
	
	/** Prints the SF Tool version information
		@memberof VersionAction
		@instance
		@return {Promise}
	*/
	run: function() {
		var packagePath = path.join(__dirname, '..', '..', '..', 'package.json');
		
		return q.ninvoke(fs, 'readFile', packagePath, {encoding: 'utf8'}).then(function(rawPkg) {
			// Parse the package
			pkg = JSON.parse(rawPkg);
			
			ui.print(chalk.bold(pkg.description));
			ui.print('version: ' + chalk.gray(pkg.version), 1);
			ui.print('repository: ' + chalk.gray(pkg.repository.url || pkg.repository), 1);
			
			return {
				version: pkg.version,
				description: pkg.description,
				website: pkg.repository.url || pkg.repository
			};
		});
	}

});