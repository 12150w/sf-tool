/**
	@class CreateAction
	@classdesc Creates a content metadata component, and adds to the project.
			If the component exists, an error is displayed
	@extends ConnectionAction
*/
var q = require('q'),
	ui = require('../../base/ui'),
	typesRegistry = require('../../metadata/types/registry'),
	chalk = require('chalk'),
	readline = require('readline'),
	ConnectionAction = require('./types/connection-action'),
	Package = require('../../base/package'),
	UIError = require('../../base/errors').UIError,
	os = require('os'),
	AdmZip = require('adm-zip');

module.exports = ConnectionAction.extend({
	key: 'create',
	description: 'Creates a new metadata component and adds it to the project',
	options: {
		'-a': 'Allows entry of optional metadata fields',
		'--localOnly': 'Only creates the component on the hard drive, not in SalesForce'
	},
	examples: {
		'sf create ApexClass SomeClass': 'Creates and adds the class named "SomeClass"',
		'sf create ApexTrigger SomeTrigger': 'Creates and adds the trigger "SomeTrigger" for SObject "Custom_Object__c"',
		'sf create ApexPage SomePage': 'Creates and adds the page "SomePage"',
		'sf create StaticResource SomeResource': 'Creates and adds the resource "SomeResource"',
		'sf create SFZipFile SomeFolder': 'Creates and adds the static resource folder "SomeFolder"',
		'sf create SFEmberApp SomeApp': 'Creates and adds the static resource file "SomeApp.js". Creates the ember folder "SomeApp"'
	},
	
	/** Tries to create the content and then add it to the project
		@memberof CreateAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var MetadataType, component, pack, template;
		
		var self = this;
		
		return q.fcall(function() {
			// Get the type name
			if(args.length < 1) {
				return ui.prompt('Metadata Type Name', {
					required: true,
					completer: typesRegistry.completer
				});
			} else {
				return args[0];
			}
			
		}).then(function(name) {
			
			// Lookup the metadata type
			return typesRegistry.forName(name);
			
		}).then(function(type) {
			
			// Get the component name
			MetadataType = type;
			if(args.length < 2) {
				return ui.prompt('ComponentName', {
					required: true,
					completer: typesRegistry.typeCompleter(MetadataType, conn)
				});
			} else {
				return args[1];
			}
			
		}).then(function(componentName) {
			// Check for component in package
			component = new MetadataType(componentName, conn.project, opts);
			
			return conn.project.containsComponent(component).then(function(exists) {
				if(exists) {
					var typeName = component.typeNameAlias != null ? component.typeNameAlias : component.typeName;
					throw new UIError(typeName + ' ' + component.fullName + ' has already been added to this project');
				}
			});
			
		}).then(function() {
			// Download the metadata to check if it exists
			var checkPack = new Package(conn),
				typeName = component.typeNameAlias != null ? component.typeNameAlias : component.typeName;
			checkPack.addComponent(component);
			
			ui.print(chalk.gray('Checking existing components...'));
			
			return checkPack.retrieve();
		
		}).then(function(result) {
			
			var zip = new AdmZip(new Buffer(result.res.zipFile, 'base64'));
			
			return component.checkForMetadata(zip);
			
		}).then(function(exists) {
			
			if(exists) {
				throw new UIError('Component already exists\n'
					+ 'Run "sf add ' + component.getProjectTypeName() + ' ' + component.fullName + '" to download'
				);
			}
			
			ui.print(chalk.green('Component does not already exist'));
			
			// Create from a template and then merge in values entered by the user
			return component.create(conn.apiVersion).then(function(template) {
				
				return self._handleTemplatePrompts(template, !!opts.a);
				
			});
			
		}).then(function(template) {
				
				// Write the file to the hard drive
				ui.print(chalk.gray('Saving to hard drive...'));
				
				return component.write(JSON.parse(template.templateString), template);
				
		}).then(function() {
				
			// Add the component to the package
			pack = new Package(conn);
			
			return pack.addComponent(component);
			
		}).then(function() {
			
			ui.print(chalk.green('Component created on hard drive'));
			ui.print(chalk.gray('Adding component to project'));
			
			// Save the component in the project
			return conn.project.addComponent(component);
			
		}).then(function() {
			
			ui.print(chalk.green('Component added to project'));
			ui.print(chalk.gray('Starting Save'));
			
			return pack.save({
				rollback: true,
				verifyOnly: false,
				tests: undefined
			}).progress(function(statusMsg) {
				// Print progress
				if(statusMsg == null) return;
				
				readline.clearLine(process.stdout, 0);
				readline.cursorTo(process.stdout, 0);
				process.stdout.write(chalk.gray(statusMsg));
				
			}).then(function(res) {
				// Go to new line
				process.stdout.write(os.EOL);
				return res;
				
			});
			
		}).then(function(deployRes) {
			// Display information about the deployment
			ui.printDeployRes(deployRes.res);
			self.showDebug(deployRes.headers);
			
			if(deployRes.res.success !== true) {
				throw new UIError('\nFailed to create component on Server\n\n'
					+ 'Run "sf save ' + component.getProjectTypeName() + ':' + component.fullName + '" to attempt saving again'
				);
			}
			
			ui.print(chalk.green('Done'));
			
		});
	},
	
	/** Goes through a list of user prompts and merges the answer into the template string
		@memberof CreateAction
		@instance
		@private
		@return {Promise | null}
		@param {Object} template The metadata file template
		@param {boolean} askAll Asks all prompt questions, not just the required ones
	*/
	_handleTemplatePrompts: function(template, askAll) {
		template.prompts = template.prompts || [];
		
		// If there are no more prompts, stop recursing
		if(template.prompts.length === 0) {
			return template;
		}
		
		// Pull the next prompt out of the list
		var prompt = template.prompts.shift();
			
		// If the prompt is not required, then just merge the default and move on
		if(!prompt.required && !askAll) {
			template.templateString = template.templateString.replace('{{' + prompt.replace + '}}', prompt.default);
			
			return this._handleTemplatePrompts(template, askAll);
		}
		
		// Get the next prompt and ask the user for the value
		var self = this;
		return ui.prompt(prompt.text, { required: true }).then(function(value) {
			
			// Merge the value into the template string
			template.templateString = template.templateString.replace('{{' + prompt.replace + '}}', value);
			
			// Get the next prompt
			return self._handleTemplatePrompts(template, askAll);
			
		});
	}
	
});