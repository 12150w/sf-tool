/**
	@class DeployAction
	@classdesc  Moves components from one connection to another
	@extends ConnectionAction
*/
var ConnectionAction = require('./types/connection-action'),
	SaveAction = require('./save'),
	Package = require('../../base/package'),
	Connection = require('../../connection/connection'),
	UIError = require('../../base/errors').UIError,
	JSZip = require('jszip'),
	yaml = require('js-yaml'),
	readline = require('readline'),
	os = require('os'),
	chalk = require('chalk'),
	q = require('q'),
	fs = require('fs-extra'),
	ui = require('../../base/ui');

module.exports = ConnectionAction.extend({
	key: 'deploy',
	description: 'Deploys metadata from one connetion to another',
	examples: {
		'sf deploy --conn fromConnection --to toConnection sets/some-set.yaml': 'Deploys some-set.yaml from "fromConnection" to "toConnection"'
	},
	options: {
		'-v': SaveAction.prototype.options['-v'],
		'--test': SaveAction.prototype.options['--test'],
		'--to': 'The name of the connection to deploy to (defaults to "default")',
		'--backup': 'The path to the backup file (to create or overwrite)',
		'-y': 'Skip deploy verification (run without user prompt)'
	},
	
	/** Deploys metadata components from one connection to another (usually a sandbox to a production)
		@memberof DeployAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var self = this,
			toConn = null,
			pack = new Package(conn, true);
		
		// Check for backup
		var backupZip = null,
			backupData = {};
		if(!!opts.backup) {
			backupZip = new JSZip();
		}
		
		return q.fcall(function() {
			// Load the to connection
			if(!opts.to) {
				toConn = conn;
				return;
			}
			return self.getProject().then(function(proj) {
				toConn = new Connection(proj, opts.to);
			});
			
		}).then(function() {
			// Verify deployment
			backupData.from = conn.getName();
			backupData.to = toConn.getName();
			backupData.verify = false;
			backupData.timestamp = new Date();
			backupData.tests = opts.test;
			
			if(opts.v === true) {
				backupData.verify = true;
				return ui.print(chalk.yellow('Verify only, changes will be rolled back'));
			}
			if(opts.y !== true) {
				return ui.verify(
					'Are you sure you want to deploy from ' + conn.getName() + ' to ' + toConn.getName() + '?'
				);
			}
			
		}).then(function() {
			// Add components from arguments
			return pack.addArgs(args);
			
		}).then(function() {
			// Download the package from the "from" connection
			ui.print('Loading deployment package from ' + conn.getName());
			return pack.retrieve();
			
		}).then(function(getData) {
			// Send the package to the "to" connection
			ui.print('Finished loading deployment package');
			var pkgBuffer = new Buffer(getData.res.zipFile, 'base64');
			if(backupZip != null) {
				backupZip.file('from-backup.zip', getData.res.zipFile, {base64: true});
			}
			var toPack = new Package(toConn, true);
			
			// Backup if needed
			var backupPromise = q();
			if(backupZip != null) {
				ui.print('Loading backup package from ' + toConn.getName());
				backupPromise = toPack.addArgs(args).then(function() {
					return toPack.retrieve();
				}).then(function(toData) {
					ui.print('Finished loading backup package');
					backupZip.file('to-backup.zip', toData.res.zipFile, {base64: true});
				});
			}
			
			return backupPromise.then(function() {
				ui.print('Deploying package to ' + toConn.getName());
				return toPack.send(pkgBuffer, {
					rollback: true,
					verifyOnly: opts.v,
					tests: opts.test,
					singlePackage: false
					
				}).progress(function(statusMsg) {
					// Print progress
					if(statusMsg == null) return;
					
					readline.clearLine(process.stdout, 0);
					readline.cursorTo(process.stdout, 0);
					process.stdout.write(chalk.gray(statusMsg));
					
				}).then(function(res) {
					// Go to new line
					process.stdout.write(os.EOL);
					return res;
					
				});
			});
			
		}).then(function(deployData) {
			// Save the backup if needed
			if(backupZip == null) {
				return deployData;
			}
			if(deployData.res.success != true) {
				return deployData;
			}
			
			// Add debug log if available
			if(deployData.headers != null && deployData.headers.DebuggingInfo != null && deployData.headers.DebuggingInfo.debugLog != null) {
				backupZip.file('deploy.log', deployData.headers.DebuggingInfo.debugLog);
			}
			
			// Add info
			backupData.result = deployData.res;
			backupZip.file('info.yaml', yaml.safeDump(backupData, {
				skipInvalid: true
			}));
			
			return backupZip.generateAsync({
				compression: 'DEFLATE',
				type: 'nodebuffer'
			}).then(function(zipBuffer) {
				return q.nfcall(fs.outputFile, opts.backup, zipBuffer)
			}).then(function() {
				return deployData;
			});
			
		}).then(function(deployData) {
			// Print the deployment results
			ui.printDeployRes(deployData.res);
			self.showDebug(deployData.headers);
			
			if(deployData.res.success != true) {
				throw new UIError('Deployment failed');
			}
			
		});
	}
	
});