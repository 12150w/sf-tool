/**
	@class DeleteAction
	@classdesc Deletes a metadata component from the hard disk and from a project connection
	@extends ConnectionAction
*/
var ConnectionAction = require('./types/connection-action'),
	UIError = require('../../base/errors').UIError,
	Package = require('../../base/package'),
	q = require('q'),
	readline = require('readline'),
	typesRegistry = require('../../metadata/types/registry'),
	yaml = require('js-yaml'),
	chalk = require('chalk'),
	os = require('os'),
	ui = require('../../base/ui');

module.exports = ConnectionAction.extend({
	key: 'delete',
	description: 'Deletes a component from SalesForce, removes it from the project and deletes the file(s) from the hard drive',
	examples: {
		'sf delete ApexClass AClass': 'Deletes a class named "AClass"',
		'sf delete ApexClass AClass --conn=production': 'Deletes a class named "AClass" using the production connection'
	},
	options: {
		'--localOnly': 'Only removes the component from the project and hard drive, not from SalesForce'
	},
	
	/** Tries to delete the component from the hard disk and from the server
		@memberof DeleteAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		var self = this,
			deletePack = new Package(conn),
			pack = new Package(conn, opts.min),
			component;
		
		return q.fcall(function() {
			// Get the type name
			if(args.length < 1) {
				return ui.prompt('Metadata Type Name', {
					required: true,
					completer: typesRegistry.completer
				});
			} else {
				return args[0];
			}
			
		}).then(function(name) {
			// Lookup the metadata type
			return typesRegistry.forName(name);
			
		}).then(function(type) {
			// Get the component name
			MetadataType = type;
			if(args.length < 2) {
				return ui.prompt('ComponentName', {
					required: true,
					completer: typesRegistry.typeCompleter(MetadataType, conn)
				});
			} else {
				return args[1];
			}
			
		}).then(function(componentName) {
			// Check for component in package
			component = new MetadataType(componentName, conn.project);
			
			if(typeof component.remove !== 'function') {
				throw new UIError('Action "delete" is not supported by this type');
			}
			
			return conn.project.containsComponent(component).fail(function() {
				throw new UIError(typeName + ' ' + component.fullName + ' does not exist in this project')
			});
			
		}).then(function() {
			// If the localOnly option is set, don't remove from the SalesForce
			if(!!opts.localOnly) {
				return q();
			}
			
			// Verify deleting files from SalesForce
			return ui.verify(
				'This will delete ' + component.fullName
				+ ' from the SalesForce connection: ' + conn.getName()
				+ ', continue?'
			).then(function() {
				
				deletePack = new Package(conn, false, true);
				
				// Add component from arguments
				return deletePack.addArgs([ component.typeName + ':' + component.fullName ]);
				
			}).then(function() {
				
				// Save the package
				ui.print(chalk.gray('Starting Delete...'));
				
				return deletePack.save({
					rollback: true,
					verifyOnly: false,
					tests: undefined
				}).progress(function(statusMsg) {
					// Print progress
					if(statusMsg == null) return;
					
					readline.clearLine(process.stdout, 0);
					readline.cursorTo(process.stdout, 0);
					process.stdout.write(chalk.gray(statusMsg));
					
				});
				
			}).then(function(deployRes) {
				// Go to new line
				process.stdout.write(os.EOL);
				
				// Display information about the deployment
				ui.printDeployRes(deployRes.res);
				self.showDebug(deployRes.headers);
				
				if(deployRes.res.success != true) {
					throw new UIError('Delete failed');
				}
				
			});
		
		}).then(function() {
			
			// Verify deleting files from hard drive
			return ui.verify(
				'This will delete ' + component.fullName + ' from the hard drive, continue?'
			);
		
		}).then(function() {
			
			// Delete the metadata file(s) from the hard drive
			return component.remove();
			
		}).then(function() {
			
			// Remove the metadata from the project settings
			return conn.project.removeComponent(component);
			
		}).then(function() {
			
			return ui.print(chalk.green('Deleted from hard drive'));
			
		});
		
	}
	
});
