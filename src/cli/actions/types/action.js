/**
	@class Action
	@classdesc The base class that all sf tool actions inherit from
*/
var Class = require('level2-base').Class,
	errors = require('../../../base/errors'),
	q = require('q');

module.exports = Class.extend({
	/** The unique name to identify the action by
		@memberof Action
		@instance
	*/
	key: null,
	
	/** A description of what the action does
		@memberof Action
		@instance
	*/
	description: null,
	
	/** A list of command line arguments for the action
		@memberof Action
		@instance
	*/
	arguments: null,
	
	/** A list of command line options for the action
		@memberof Action
		@instance
	*/
	options: null,
	
	/** A map of example commands and their comments
		@memberof Action
		@instance
	*/
	examples: null,
	
	// construct an action and get shared options
	init: function() {
		this._sharedOpts = this.getSharedOptions();
	},
	
	/** Executes the logic for the action
		@memberof Action
		@instance
		@virtual
		@param {string[]} args - Command line arguments
		@param {object} options - Command line named options
		@return {Promise}
	*/
	run: function(args, options) {
		return q.fcall(function() {
			throw new errors.NotImplementedError('Action.run() is not implemented');
		});
	},
	
	/** Returns an array of options that are common for one or more actions
		@memberof Action
		@instance
		@virtual
		@return {Object[]} The shared options for running the command
	*/
	getSharedOptions: function() {
		return [];
	},
	
	/** Internal proxy for executing the action
		@memberof Action
		@private
	*/
	_runProxy: function() {
		return this.run.apply(this, arguments);
	}
	
});