/**
	@class ProjectAction
	@classdesc An action that requires a project in order to run
	@extends Action
*/
var Action = require('./action'),
	Project = require('../../../base/project'),
	UIError = require('../../../base/errors').UIError,
	q = require('q');

module.exports = Action.extend({
	
	// constructs and sets the project reference to null
	init: function() {
		this._super.apply(this, arguments);
		this._proj = null;
	},
	
	/** Creates a project and stores it or returns the cached project
		@memberof ProjectAction
		@instance
		@return {Project} The project instance
	*/
	getProject: function() {
		var self = this;
		if(self._proj != null) return q(self._proj);
		
		self._proj = new Project();
		
		return self._proj.exists().then(function(exists) {
			if(exists !== true) throw new UIError('No project found');
			else return self._proj;
		});
	}
	
});
