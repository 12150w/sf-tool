/**
	@class ConnectionAction
	@classdesc Action with connection context
	@extends ProjectAction
*/
var ProjectAction = require('./project-action'),
	Project = require('../../../base/project'),
	Connection = require('../../../connection/connection'),
	ui = require('../../../base/ui'),
	os = require('os'),
	q = require('q'),
	chalk = require('chalk'),
	UIError = require('../../../base/errors').UIError;

var LOG_CATEGORIES = [
	'Db', 'Workflow', 'Validation', 'Callout', 'Apex_code', 'Apex_profiling', 'Visualforce', 'System', 'All'
];
var LOG_LEVELS = [
	'NONE', 'ERROR', 'WARN', 'INFO', 'DEBUG', 'FINE', 'FINER', 'FINEST'
];

module.exports = ProjectAction.extend({
	
	/** Adds connection options and returns an array of options that are common for one or more actions
		@memberof ConnectionAction
		@instance
		@return {Object[]} The shared options for running the command
	*/
	getSharedOptions: function() {
		var groups = this._super.apply(this, arguments);
		groups.push({
			'--conn': 'The connection name to use (defaults to "default")',
			'--log': 'Adds a debug log filter to the command with the format CATEGORY:LEVEL',
			'--logDebug': 'Adds debug log filters but only shows user debug statements'
		});
		
		return groups;
	},
	
	// construct and set the connection instance to null
	init: function() {
		this._super.apply(this, arguments);
		this._conn = null;
	},
	
	/** Gets the connection from the project and stores it
		@memberof ConnectionAction
		@instance
		@return {Promise}
	*/
	getConnection: function(options) {
		var self = this;
		if(self._conn != null) return self._conn;
		
		// Initialize the connection
		return self.getProject().then(function(proj) {
			self._conn = new Connection(proj, options.conn || process.env.SF_CONN);
			return self._conn;
			
		}).then(function() {
			// Set logging
			var logCategories = options.log || [];
			if(Object.prototype.toString.call(logCategories) !== '[object Array]') {
				logCategories = [logCategories];
			}
			if(options.logDebug != null) {
				logCategories.push('Apex_code:DEBUG');
				self._onlyLogDebug = true;
			}
			
			// Parse log categories
			logCategories = logCategories.map(function(categoryIdentifier) {
				var catMatch = /^(.+):(.+)$/.exec(categoryIdentifier);
				
				if(catMatch == null) {
					throw new UIError('Invalid log identifier ' + categoryIdentifier);
				}
				if(LOG_CATEGORIES.indexOf(catMatch[1]) < 0) {
					throw new UIError('Invalid log category ' + catMatch[1] + os.EOL + 'Valid categories: ' + LOG_CATEGORIES.join(', '));
				}
				if(LOG_LEVELS.indexOf(catMatch[2]) < 0) {
					throw new UIError('Invalid log level ' + catMatch[2] + os.EOL + 'Valid levels: ' + LOG_LEVELS.join(', '));
				}
				
				return {
					'tns:category': catMatch[1],
					'tns:level': catMatch[2]
				};
			});
			
			if(logCategories.length < 1) {
				return self._conn;
			}
			
			var loggingHeader = {
				'tns:categories': logCategories
			};
			self._hasLogging = true;
			
			return self._conn.meta.setHeader('tns:DebuggingHeader', loggingHeader).then(function() {
				return self._conn.apex.setHeader('tns:DebuggingHeader', loggingHeader);
			}).then(function() {
				return self._conn;
			});
		});
	},
	
	/** Prints the debug information
		@memberof ConnectionAction
		@instance
		@param {Object} headers - The debug information headers
	*/
	showDebug: function(headers) {
		var self = this;
		if(this._hasLogging !== true) {
			return;
		}
		
		// Check for no debug
		if(headers == null || headers.DebuggingInfo == null || headers.DebuggingInfo.debugLog == null) {
			return ui.print(chalk.yellow('No debug log found in response'));
		}
		
		// Print the log
		this._showRawLog(headers.DebuggingInfo.debugLog);
	},
	
	/** Queries and prints the log using the tooling API
		@memberof ConnectionAction
		@instance
		@param {string} logId - The ID of the debug log
		@return {Promise}
	*/
	showToolingDebug: function(logId) {
		if(this._hasLogging !== true) return q.fcall(function() {});
		if(logId == null) {
			return ui.print(chalk.yellow('No debug log found in response'));
			return q.fcall(function() {});
		}
		var self = this;
		
		return q.fcall(function() {
			// Query the log body
			return self._conn.sfRequest('/services/data/v38.0/tooling/sobjects/ApexLog/' + logId + '/Body/', 'GET');
			
		}).then(function(logRes) {
			// Print the log
			self._showRawLog(logRes.body.toString('utf8'));
			
		});
	},
	
	/** Prints log text
		@memberof ConnectionAction
		@private
		@param {string} logText - The raw text to print
	*/
	_showRawLog: function(logText) {
		var self = this;
		
		ui.print('');
		ui.print(chalk.bold('Debug Log'));
		ui.print('');
		
		if(logText == null) return;
		
		logText.split(/\r?\n/).forEach(function(line) {
			var userMatch = /USER_DEBUG/.exec(line);
			
			if(self._onlyLogDebug === true) {
				if(userMatch == null) return;
				else return ui.print(line);
			}
			
			if(userMatch != null) {
				ui.print(chalk.blue(line));
			} else {
				ui.print(line);
			}
		});
	},
	
	/** Internal proxy for executing the action; Gets the connections first
		@memberof ConnectionAction
		@private
		@param {string[]} args - command line args
		@param {Object} options - command line options
	*/
	_runProxy: function(args, options) {
		var self = this;
		
		return this.getConnection(options).then(function(conn) {
			return self.run(args, options, conn);
		});
	}
	
});
