/**
	@class ReAuthAction
	@classdesc Allows re-authentication of a connection
	@extends ConnectionAction
*/
var electronRunner = require('../../electron/runner'),
	ui = require('../../base/ui'),
	chalk = require('chalk'),
	ConnectionAction = require('./types/connection-action'),
	UIError = require('../../base/errors').UIError;

module.exports = ConnectionAction.extend({
	key: 'reauth',
	description: 'Refreshes the authorization for a connection',
	examples: {
		'sf reauth': 'Refresh the default connection',
		'sf reauth --conn=production': 'Refresh the connection named "production"'
	},
	
	/** Opens a browser window and asks for credentials for a connection
		@memberof ReAuthAction
		@instance
		@param {string[]} args - Command line arguments
		@param {object} opts - Command line named options
		@param {Connection} conn - The connection being used
		@return {Promise}
	*/
	run: function(args, opts, conn) {
		ui.print('Refreshing connection "' + conn.getName() + '"');
		
		// Load the authorization data
		return conn.loadConfig().then(function(data) {
			// Request authorization
			return electronRunner.run('authorize', {
				loginServer: data.login_server,
				limitedAccess: data.limited
			});
			
		}).then(function(oauth) {
			// Update the authorization
			if(oauth == null) {
				throw new UIError('no authorization data');
			}
			if(oauth.error != null) {
				throw new UIError(oauth.error);
			}
			
			return conn.storeAuthorization(oauth);
			
		}).then(function() {
			ui.print(chalk.bold(chalk.green('Successfully refreshed authorization')));
			
		});
		
	}
	
});
