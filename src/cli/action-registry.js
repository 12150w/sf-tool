/**
	@module actionRegistry
*/
var glob = require('glob'),
	Class = require('level2-base').Class;

var actionFolder = __dirname + '/actions',
	actionFiles = glob.sync(actionFolder + '/**.js', { cwd: actionFolder }),
	cache = {};

/** @memberof actionRegistry */
var Registry = Class.extend({
	
	// Import all actions
	init: function() {
		var action, importPath;
		for(var i=0; i<actionFiles.length; i++) {
			importPath = actionFiles[i].replace(actionFolder, './actions');
			action = new (require(importPath))();
			
			cache[action.key] = action;
		}
	},
	
	/** Gets the ui action
		@param {string} actionName - The name of the action to retrieve
		@return {Action} The action from the cache
	*/
	get: function(actionName) {
		var action = cache[actionName];
		if(!action) {
			throw 'Invalid action: ' + actionName;
		}
		
		return action;
	},
	
	/** Gets all the action keys
		@return {string[]} The action cache keys
	*/
	getKeys: function() {
		return Object.keys(cache);
	}
	
});

module.exports = new Registry();