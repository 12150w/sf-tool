// Generated via build-types script
var Metadata = require('../../metadata');

module.exports = Metadata.extend({
	"typeName": "LightningBolt",
	"extension": ".lightningBolt",
	"folder": "lightningBolts"
});