// Generated via build-types script
var Metadata = require('../../metadata');

module.exports = Metadata.extend({
	"typeName": "PlatformCachePartition",
	"extension": ".cachePartition",
	"folder": "cachePartitions"
});