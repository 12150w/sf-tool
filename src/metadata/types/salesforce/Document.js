// Generated via build-types script
var ContentMetadata = require('../../content-metadata');

module.exports = ContentMetadata.extend({
	"typeName": "Document",
	"extension": null,
	"folder": "documents",
	"inFolders": true
});