// Generated via build-types script
var Metadata = require('../../metadata');

module.exports = Metadata.extend({
	"typeName": "SynonymDictionary",
	"extension": ".synonymDictionary",
	"folder": "synonymDictionaries"
});