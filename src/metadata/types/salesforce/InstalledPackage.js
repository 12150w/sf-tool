// Generated via build-types script
var Metadata = require('../../metadata');

module.exports = Metadata.extend({
	"typeName": "InstalledPackage",
	"extension": ".installedPackage",
	"folder": "installedPackages"
});