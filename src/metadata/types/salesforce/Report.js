// Generated via build-types script
var Metadata = require('../../metadata');

module.exports = Metadata.extend({
	"typeName": "Report",
	"extension": ".report",
	"folder": "reports",
	"inFolders": true
});