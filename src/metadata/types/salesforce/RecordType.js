// Generated via build-types script
var SubMetadata = require('../../sub-metadata');

module.exports = SubMetadata.extend({
	"typeName": "RecordType",
	"folder": "recordTypes",
	"parentTypeName": "CustomObject"
});