// Generated via build-types script
var CompoundMetadata = require('../../compound-metadata');

module.exports = CompoundMetadata.extend({
	"typeName": "Workflow",
	"extension": ".workflow",
	"folder": "workflows",
	"subTypes": {
		"rules": "WorkflowRule"
	}
});