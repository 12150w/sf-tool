// Generated via build-types script
var SubMetadata = require('../../sub-metadata');

module.exports = SubMetadata.extend({
	"typeName": "FieldSet",
	"folder": "fieldSets",
	"parentTypeName": "CustomObject"
});