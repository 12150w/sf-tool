// Generated via build-types script
var SubMetadata = require('../../sub-metadata');

module.exports = SubMetadata.extend({
	"typeName": "BusinessProcess",
	"folder": "businessProcesses",
	"parentTypeName": "CustomObject"
});