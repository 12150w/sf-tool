// Generated via build-types script
var Metadata = require('../../metadata');

module.exports = Metadata.extend({
	"typeName": "TransactionSecurityPolicy",
	"extension": ".transactionSecurityPolicy",
	"folder": "transactionSecurityPolicies"
});