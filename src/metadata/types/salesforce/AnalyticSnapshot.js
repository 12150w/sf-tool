// Generated via build-types script
var Metadata = require('../../metadata');

module.exports = Metadata.extend({
	"typeName": "AnalyticSnapshot",
	"extension": ".snapshot",
	"folder": "analyticSnapshots"
});