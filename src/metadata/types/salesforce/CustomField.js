// Generated via build-types script
var SubMetadata = require('../../sub-metadata');

module.exports = SubMetadata.extend({
	"typeName": "CustomField",
	"folder": "fields",
	"parentTypeName": "CustomObject"
});