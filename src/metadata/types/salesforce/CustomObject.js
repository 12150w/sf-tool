// Generated via build-types script
var CompoundMetadata = require('../../compound-metadata');

module.exports = CompoundMetadata.extend({
	"typeName": "CustomObject",
	"extension": ".object",
	"folder": "objects",
	"subTypes": {
		"businessProcesses": "BusinessProcess",
		"compactLayouts": "CompactLayout",
		"fieldSets": "FieldSet",
		"fields": "CustomField",
		"indexes": "Index",
		"listViews": "ListView",
		"recordTypes": "RecordType",
		"sharingReasons": "SharingReason",
		"validationRules": "ValidationRule",
		"webLinks": "WebLink"
	}
});