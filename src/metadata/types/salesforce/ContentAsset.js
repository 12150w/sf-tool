// Generated via build-types script
var ContentMetadata = require('../../content-metadata');

module.exports = ContentMetadata.extend({
	"typeName": "ContentAsset",
	"extension": ".asset",
	"folder": "contentassets"
});