// Generated via build-types script
var Metadata = require('../../metadata');

module.exports = Metadata.extend({
	"typeName": "RemoteSiteSetting",
	"extension": ".remoteSite",
	"folder": "remoteSiteSettings"
});