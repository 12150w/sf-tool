// Generated via build-types script
var ContentMetadata = require('../../content-metadata');

module.exports = ContentMetadata.extend({
	"typeName": "SiteDotCom",
	"extension": ".site",
	"folder": "siteDotComSites"
});