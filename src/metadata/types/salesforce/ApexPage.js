// Generated via build-types script
var ContentMetadata = require('../../content-metadata');

module.exports = ContentMetadata.extend({
	"typeName": "ApexPage",
	"extension": ".page",
	"folder": "pages"
});