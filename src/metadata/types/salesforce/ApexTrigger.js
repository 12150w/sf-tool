// Generated via build-types script
var ContentMetadata = require('../../content-metadata');

module.exports = ContentMetadata.extend({
	"typeName": "ApexTrigger",
	"extension": ".trigger",
	"folder": "triggers"
});