// Generated via build-types script
var SubMetadata = require('../../sub-metadata');

module.exports = SubMetadata.extend({
	"typeName": "SharingReason",
	"folder": "sharingReasons",
	"parentTypeName": "CustomObject"
});