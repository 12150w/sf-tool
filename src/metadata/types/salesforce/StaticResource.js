// Generated via build-types script
var ContentMetadata = require('../../content-metadata');

module.exports = ContentMetadata.extend({
	"typeName": "StaticResource",
	"extension": ".resource",
	"folder": "staticresources"
});