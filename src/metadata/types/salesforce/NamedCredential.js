// Generated via build-types script
var Metadata = require('../../metadata');

module.exports = Metadata.extend({
	"typeName": "NamedCredential",
	"extension": ".namedCredential",
	"folder": "namedCredentials"
});