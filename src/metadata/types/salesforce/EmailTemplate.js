// Generated via build-types script
var ContentMetadata = require('../../content-metadata');

module.exports = ContentMetadata.extend({
	"typeName": "EmailTemplate",
	"extension": ".email",
	"folder": "email",
	"inFolders": true,
	"folderTypeName": "EmailFolder"
});