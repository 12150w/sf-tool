/**
	@class SFEmberApp
	@classdesc Ember application content metadata
	@extends StaticResource
	@param {string} fullName - The name of the metadata type
	@param {Project} project - An instance of the project
	@parma {Object} options - An options hash
*/
var StaticResource = require('../salesforce/StaticResource'),
	EmberContent = require('../../content/types/SFEmberApp');

module.exports = StaticResource.extend({
	contentType: 'SFEmberApp',
	
	init: function(fullName, project, options) {
		this._super.apply(this, arguments);
		
		options = options || {};
		this._content = new EmberContent(this, options.version || null);
	}
	
});