/**
	@class SFZipFile
	@classdesc Zip folder content metadata
	@extends StaticResource
	@param {string} fullName - The name of the metadata type
	@param {Project} project - An instance of the project
	@parma {Object} options - An options hash
*/
var StaticResource = require('../salesforce/StaticResource'),
	ZipContent = require('../../content/types/SFZipFile');

module.exports = StaticResource.extend({
	contentType: 'SFZipFile',
	
	// [override] init constructs a new CompoundMetadata object
	init: function() {
		this._super.apply(this, arguments);
		this._content = new ZipContent(this);
	}
	
});