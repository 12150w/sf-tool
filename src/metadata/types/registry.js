/**
	@module metadataRegistry
	@desc Provides access to all available types given a type name
*/
var q = require('q'),
	glob = require('glob'),
	path = require('path'),
	UIError = require('../../base/errors').UIError;

var types = module.exports = {
	_registry: {},
	_listing: [],
	_capListing: [],
	
	/** Completer for names */
	completer: function(line) {
		var foundOpts = [],
			capLine = line.toUpperCase();
		
		for(var i=0; i<types._capListing.length; i++) {
			if(types._capListing[i].indexOf(capLine) === 0) {
				foundOpts.push(types._listing[i]);
			}
		}
		
		var showOpts = foundOpts.length > 0 ? foundOpts : types._listing;
		if(foundOpts.length < 1 && line.length > 0) {
			showOpts = [];
		}
		
		return [showOpts, line];
	},
	
	/** Completer for type names */
	typeCompleter: function(type, conn) {
		return function(line, callback) {
			var connNamespace = null,
				folderListing = false;
			
			conn.loadConfig().then(function(config) {
				// Gather the connection's namespace
				connNamespace = config.info.namespace;
				
				// Check for folder listing
				var typeQuery = { type: type.prototype.typeName };
				if(type.prototype.inFolders === true) {
					var folderMatch = /^(.+?)\//.exec(line);
					
					if(folderMatch == null) {
						if(type.prototype.folderTypeName != null) {
							typeQuery.type = type.prototype.folderTypeName;
						} else {
							typeQuery.type += 'Folder';
						}
						folderListing = true;
					} else {
						typeQuery.folder = folderMatch[1];
					}
				}
				
				return conn.meta.run('listMetadata', { queries: [typeQuery] });
				
			}).then(function(data) {
				if(data.res == null) return callback(null, [[], line]);
				
				var filtered = [],
					allNames = [];
				data.res.forEach(function(meta) {
					var externalName = meta.fullName;
					
					// Add forward slash if listing folders
					if(folderListing === true) {
						externalName += '/';
					}
					
					// Add namespace if needed
					if(
						!!meta.namespacePrefix
						&& meta.namespacePrefix !== connNamespace
						&& meta.fullName.indexOf('standard__') < 0
					) {
						externalName = meta.namespacePrefix + '__' + externalName;
					}
					
					allNames.push(externalName);
					if(externalName.indexOf(line) === 0) {
						filtered.push(externalName);
					}
				});
				
				var showNames = filtered.length ? filtered : allNames;
				if(filtered.length < 1 && line.length > 0) {
					showNames = [];
				}
				
				showNames.sort();
				callback(null, [showNames, line]);
				
			}, function(err) {
				throw err;
			}).done();
		};
	},
	
	/** Access type classes
		@param {string} name - The name of the metadata component
		@return {Metadata} A metadata type class
	*/
	forName: function(name) {
		if(name === null) {
			throw new UIError('Unsupported metadata type: null');
		}
		var normalizedName = name.toUpperCase();
		
		// Check the registry
		var typeDef = types._registry[normalizedName];
		if(typeDef == null) {
			throw new UIError('Unsupported metadata type: "' + name + '"');
		}
		
		return typeDef;
	}
	
};

// Register all available types
var salesforceTypesFolder = path.join(__dirname, 'salesforce'),
	customTypesFolder = path.join(__dirname, 'custom'),
	salesforceTypes = glob.sync('*.js', { cwd: salesforceTypesFolder }),
	customTypes = glob.sync('*.js', { cwd: customTypesFolder });

salesforceTypes.forEach(function(partialPath) {
	var typeDef = require('./salesforce/' + partialPath),
		key = typeDef.prototype.typeName;
	types._registry[key.toUpperCase()] = typeDef;
	
	types._capListing.push(key.toUpperCase());
	types._listing.push(key);
});

customTypes.forEach(function(partialPath) {
	var typeDef = require('./custom/' + partialPath),
		key = (typeDef.prototype.contentType || typeDef.prototype.typeName);
	
	types._registry[key.toUpperCase()] = typeDef;
	types._capListing.push(key.toUpperCase());
	types._listing.push(key);
});
