/**
	@module typeTemplates
	@desc Base source code that defines a type definition
*/

/** Base Template */
module.exports.base = "// Generated via build-types script\n\
var Metadata = require('../../metadata');\n\
\n\
module.exports = Metadata.extend({{META_INFO}});";

/** Content Template */
module.exports.content = "// Generated via build-types script\n\
var ContentMetadata = require('../../content-metadata');\n\
\n\
module.exports = ContentMetadata.extend({{META_INFO}});";

/** Compound Template */
module.exports.compound = "// Generated via build-types script\n\
var CompoundMetadata = require('../../compound-metadata');\n\
\n\
module.exports = CompoundMetadata.extend({{META_INFO}});";

/** Sub Template */
module.exports.sub = "// Generated via build-types script\n\
var SubMetadata = require('../../sub-metadata');\n\
\n\
module.exports = SubMetadata.extend({{META_INFO}});";