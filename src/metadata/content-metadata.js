/**
	@class ContentMetadata
	@classdesc A metadata that contains a content file
	@extends Metadata
	@param {string} fullName The name of the Metadata type
	@param {Project} project An instance of a SF Tool project
*/
var Metadata = require('./metadata'),
	contentRegistry = require('./content/registry'),
	NotImplementedError = require('../base/errors').NotImplementedError,
	FileChain = require('./filechain'),
	path = require('path'),
	xml = require('../base/xml'),
	fs = require('fs-extra');

module.exports = Metadata.extend({
	typeName: 'ContentMetadata',
	
	// [override] init constructs a new CompoundMetadata object
	init: function() {
		this._super.apply(this, arguments);
		
		// Initialize the content object based on the type name
		var Content = contentRegistry.forName(this.typeName);
		this._content = new Content(this);
	},
	
	/** Reads the metadata and the content from the disk
		@memberof ContentMetadata
		@instance
		@virtual
		@return {Promise | Object} Object repesentation of the metadata
	*/
	read: function() {
		// Read the metadata file
		var self = this,
			metadata;
		
		return q.nfcall(fs.readFile, this.getFilePath(), { encoding: 'utf8' }).then(function(metadataJSON) {
			metadata = JSON.parse(metadataJSON);
			
			// Read the content file
			return self._content.read();
			
		}).then(function(file) {
			
			return { metadata: metadata, content: file };
			
		}).fail(function(reason) {
			// If the file doesn't exist, pass back null; otherwise propogate the error
			if(reason.code === 'ENOENT') {
				return null;
			} else {
				throw reason;
			}
		});
		
	},
	
	/** Writes the metadata and the content to the disk
		@memberof ContentMetadata
		@instance
	*/
	write: function(data, template) {
		var self = this,
			folderPath = path.join(this._project.path(), 'sf', this.getFolderName()),
			metaPath = path.join(folderPath, this.fullName + '-meta.json');
		
		// Remove xmlns and content
		delete data['$'];
		var content = data.content;
		delete data.content;
		
		// Save the metadata
		return q.nfcall(fs.outputFile, this.getFilePath(), JSON.stringify(data, null, '   '), {
			encoding: 'utf8'
			
		}).then(function() {
			// Save the content
			return self._content.write(content, template);
			
		});
	},
	
	/** Deletes the metadata file and the content file from the computer's hard disk
		@memberof ContentMetadata
		@instance
		@virtual
	*/
	remove: function() {
		var self = this;
		return q.nfcall(fs.unlink, this.getFilePath()).then(function() {
			
			return self._content.remove();
			
		});
	},
	
	// [override] compress reads the metadata and its dependencies, merges them into one and then adds it to the zip file
	// compress also reads the content and its dependencies, takes the "highest level" and adds it to the zip file;
	// returns a promise
	/** Rreads the metadata and its dependencies, merges them into one and then adds it to the zip file.
		Compress also reads the content and its dependencies, takes the "highest level" and adds it to the zip file;
		
		@memberof ContentMetadata
		@instance
		@return {null}
		@param {AdmZip} zip An instance of node adm-zip
	*/
	compress: function(zip) {
		var self = this;
		return new FileChain(this).merge().then(function(data) {
			
			// Convert the metadata to XML
			var metaXml = xml.build(self.typeName, data.metadata);
			
			// Add the metadata and content files to the zip file
			zip.file(self.getCompressPath(), new Buffer(metaXml, 'utf8'));
			zip.file(self._content.getCompressPath(), data.content);
			
		});
	},
	
	/** Pulls the metdata and content from a zip file
		@memberof ContentMetadata
		@instance
		@return {Promise | Object} Object representation of metadata and content
		@param {AdmZip} zip An instance of node adm-zip
	*/
	extract: function(zip) {
		// Read the source and meta
		var data = {},
			self = this;
		
		return this._extractFile(zip, this.getExtractPath()).then(function(metaSrc) {
			// Parse the xml
			if(metaSrc == null) {
				throw new Error('Meta for ' + self.typeName + ' ' + self.fullName + ' not found at ' + self.getExtractPath());
			}
			
			return xml.parse(metaSrc.toString('utf8'));
			
		}).then(function(srcMeta) {
			// Read the source
			data = srcMeta;
			
			return self._content.extract(zip);
			
		}).then(function(contentData) {
			
			// Add the content to the data
			data.content = contentData;
			
			return data;
		});
	},
	
	/** Generates an object representation of metadata with the minimum required properties
		@memberof ContentMetadata
		@instance
		@virtual
		@param {string} apiVersion - The version of the SalesForce API being used
		@return {Object} Returns an object representing a metadata type
	*/
	create: function(apiVersion) {
		var typeName = this._content.typeName || this.typeName,
			templatePath = path.join(__dirname, 'templates',  typeName + '.js'),
			requirePath = './templates/' + typeName + '.js';
		
		// Check for the existance of the template file
		var self = this,
			template;
		return q.nfcall(fs.lstat, templatePath).then(function() {
			
			// Load the template file
			return q.fcall(require, requirePath);
			
		}).then(function(template) {
			
			// Merge in the component information
			template.templateString = (JSON.stringify(template.templateString, null, '	') || '')
				.replace('{{apiVersion}}', apiVersion)
				.replace('{{fullName}}', self.getFullName())
				.replace('{{typeName}}', self.typeName);
			
			return template;
			
		}).fail(function() {
			
			throw new NotImplementedError('No metadata template for this type');
			
		});
	},
	
	/** Gets the file path to this metadata resource on the disk
		@memberof ContentMetadata
		@instance
		@return {string} The system path to the metadata file
	*/
	getFilePath: function() {
		return path.join(this.getFolderPath(), this.getFullName() + '-meta.json');
	},
	
	/** Gets the path to where the metadata XML is stored in a deploy package
		@memberof ContentMetadata
		@instance
		@return {string} The zip file directory path to store the metadata at
	*/
	getCompressPath: function() {
		return (!!this.inFolders ? this.folder + '/' + this.containingFolder : this.folder)
			+ '/' + this.getPackagedName() + '-meta.xml';
	},
	
	/** Gets the typeName of this component in the project's settings YAML file
		If the typeName of the content is defined, then this is used instead
		@memberof ContentMetadata
		@instance
		@return {string} The typeName
	*/
	getProjectTypeName: function() {
		return  this._content.typeName || this.typeName;
	}
	
});
