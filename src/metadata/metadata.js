/**
	@class Metadata
	@classdesc Base metadata model
	@param {string} fullName The name of the Metadata type
	@param {Project} project An instance of a SF Tool project
*/
var Class = require('level2-base').Class,
	path = require('path'),
	q = require('q'),
	fs = require('fs-extra'),
	NotImplementedError = require('../base/errors').NotImplementedError,
	Project = require('../base/project'),
	FileChain = require('./filechain'),
	xml = require('../base/xml');

module.exports = Class.extend({
	needsPrefix: true,
	syncDisabled: false,
	inFolders: false,
	folderListing: true,
	
	// Constructs a new Metadata object
	init: function(fullName, project) {
		this.fullName = fullName;
		this.isProduction = false;
		this._project = project;
		
		// Check for foldered name
		if(this.inFolders === true) {
			var folderMatch = /^(.+?)\/(.+)$/.exec(fullName);
			
			if(folderMatch == null) {
				throw 'Metadata name "' + fullName + '" does not contain folder name';
			}
			
			this.fullName = folderMatch[2];
			this.containingFolder = folderMatch[1];
		}
	},
	
	/** Reads the metadata from the disk
		@memberof Metadata
		@instance
		@virtual
		@return {Promise | Object} Object repesentation of the metadata
	*/
	read: function() {
		var self = this;
		return q.nfcall(fs.readFile, this.getFilePath(), { encoding: 'utf8' }).then(function(metadataJSON) {
			
			return JSON.parse(metadataJSON);
			
		}).fail(function(reason) {
			// If the file doesn't exist, pass back null; otherwise propogate the error
			if(reason.code === 'ENOENT') {
				return null;
			} else {
				throw reason;
			}
		});
		
	},
	
	/** Writes the metadata file to the disk
		@memberof Metadata
		@instance
		@return {Promise} A promise that resolves when the file has been written
		@param {Object} data Object representation of the metadata
	*/
	write: function(data) {
		// Remove xmlns
		delete data['$'];
		
		return q.nfcall(
			fs.outputFile,
			this.getFilePath(),
			JSON.stringify(data, null, '	'),
			{ encoding: 'utf8' }
		);
	},
	
	/** Deletes the metadata file from the disk
		@memberof Metadata
		@instance
		@virtual
		@return {NotImplementedError} Throws an error because it is not implemented by default
	*/
	remove: function() {
		throw new NotImplementedError('Remove is not implemented for this metadata type');
	},
	
	/** Reads the metadata and any dependencies, merges them, then adds them to a zip file
		@memberof Metadata
		@instance
		@return {null}
		@param {AdmZip} zip An instance of node adm-zip
	*/
	compress: function(zip) {
		var self = this;
		
		return new FileChain(this).merge().then(function(mergedMetadata) {
			var metaXml = xml.build(self.typeName, mergedMetadata),
				zipPath = self.getCompressPath();
			
			zip.file(zipPath, new Buffer(metaXml, 'utf8'));
			
		});
	},
	
	/** Pulls the metdata out of a zip file
		@memberof Metadata
		@instance
		@return {Promise | Object} Object representation of metadata
		@param {AdmZip} zip An instance of node adm-zip
	*/
	extract: function(zip) {
		var self = this;
		
		// Read the xml
		return this._extractFile(zip, this.getExtractPath()).then(function(xmlSrc) {
			
			// Parse the xml
			if(xmlSrc == null) {
				throw new Error(self.typeName + ' ' + self.fullName + ' not found in package (' + self.getExtractPath() + ')');
			}
			return xml.parse(xmlSrc.toString('utf8'));
			
		});
	},
	
	/** Checks for the metadata in a zip file
		@memberof Metadata
		@instance
		@return {Promise|boolean} If false, an error is thrown. Otherwise, returns true
		@param {AdmZip} zip An instance of node adm-zip
	*/
	checkForMetadata: function(zip) {
		// Read the xml
		return this._extractFile(zip, this.getExtractPath()).then(function(xmlSrc) {
			
			return xmlSrc !== null && xmlSrc !== undefined;
			
		});
	},
	
	/** Creates a new Metadata file
		@memberof Metadata
		@instance
		@virtual
		@return {NotImplementedError} Throws an error because it is not implemented by default
	*/
	create: function() {
		throw new NotImplementedError('create() not supported for this type');
	},
	
	/** Gets the full name of the metadata instance
		@memberof Metadata
		@instance
		@return {string} The unique name for the metadata instance
	*/
	getFullName: function() {
		return this.fullName;
	},
	
	/** Gets the folder name (relative to '<project directory>/sf') for this metadata file.
		If the metadata has a containing folder, it is joined to the folder with path.join()
		@memberof Metadata
		@instance
		@return {string} The name of the folder
	*/
	getFolderName: function() {
		return !!this.inFolders ? path.join(this.folder, this.containingFolder) : this.folder;
	},
	
	/** Gets the name of the file in the package
		@memberof Metadata
		@instance
		@return {string} The fullname and extension
	*/
	getPackagedName: function() {
		return this.getFullName() + (!!this.extension ? this.extension : '');
	},
	
	/** Gets the typeName of this component in the project's settings YAML file
		@memberof Metadata
		@instance
		@return {string} The typeName
	*/
	getProjectTypeName: function() {
		return  this.typeName;
	},
	
	/** Gets the fullName of this component in the project's settings YAML file
		@memberof Metadata
		@instance
		@return {string} The fullName with /<containing folder> prepended
	*/
	getProjectStoreName: function() {
		return !!this.inFolders ? this.containingFolder + '/' + this.fullName : this.fullName;
	},
	
	/** Gets the path to the folder that contains the metadata file
		@memberof Metadata
		@instance
		@return {string} The system path to the folder
	*/
	getFolderPath: function() {
		return path.join(this._project.path(), 'sf', this.getFolderName());
	},
	
	/** Gets the file path to this metadata resource on the disk
		@memberof Metadata
		@instance
		@return {string} The system path to the metadata file
	*/
	getFilePath: function() {
		return path.join(this.getFolderPath(), this.fullName + '.json');
	},
	
	/** Gets the path to where the metadata XML is stored in a deploy package
		@memberof Metadata
		@instance
		@return {string} The zip file directory path to store the metadata at
	*/
	getCompressPath: function() {
		return (!!this.inFolders ? this.folder + '/' + this.containingFolder : this.folder)
			+ '/' + this.getPackagedName();
	},
	
	/** Gets the path to where the metadata XML is stored in a retrieve package
		@memberof Metadata
		@instance
		@return {string} The zip file directory path to retrieve the metadata from
	*/
	getExtractPath: function() {
		return 'unpackaged/' + this.getCompressPath();
	},
	
	/** Reads a file from a zip archive
		@memberof Metadata
		@instance
		@private
		@return {Buffer | promise} A Buffer representing a file
	*/
	_extractFile: function(zip, dataPath) {
		var readDefer = q.defer();
		
		zip.readFileAsync(dataPath, function(data) {
			readDefer.resolve(data);
		});
		
		return readDefer.promise;
	}
		
});