module.exports = {
	prompts: [
		{
			text: 'Status (Active or Inactive)',
			replace: 'status',
			required: false,
			default: 'Active'
		}
	],
	
	templateString: {
		"apiVersion": "{{apiVersion}}",
		"content": "public class {{fullName}} {}",
		"status": "{{status}}"
	}
};