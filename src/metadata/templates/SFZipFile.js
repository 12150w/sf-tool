module.exports = {
	prompts: [
		{
			text: 'Cache Control (Private or Public)',
			replace: 'cache',
			required: false,
			default: 'Private'
		}
	],

	templateString: {
		"cacheControl": "{{cache}}",
		"contentType": "application/zip",
		"content": ""
	}
};