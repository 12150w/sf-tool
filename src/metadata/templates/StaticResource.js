module.exports = {
	prompts: [
		{
			text: 'Cache Control (Private or Public)',
			replace: 'cache',
			required: false,
			default: 'Private'
		},
		{
			text: 'MIME Type',
			replace: 'contentType',
			required: true,
			default: 'text/plain'
		}
	],
	
	templateString: {
		"cacheControl": "{{cache}}",
		"contentType": "{{contentType}}",
		"content": ""
	}
};