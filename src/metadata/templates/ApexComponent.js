module.exports = {
	templateString: {
		"apiVersion": "{{apiVersion}}",
		"label": "{{fullName}}",
		"content": "<apex:component></apex:component>"
	}
};