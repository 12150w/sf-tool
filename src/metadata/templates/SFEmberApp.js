module.exports = {
	prompts: [
		{
			text: 'Cache Control (Private or Public)',
			replace: 'cache',
			required: false,
			default: 'Private'
		}
	],

	templateString: {
		"cacheControl": "{{cache}}",
		"contentType": "application/javascript",
		"content": ""
	},

	appTemplateString: "define('app', ['l2:app'], function(App) {\n\treturn App.create({\n\t\trootElement: '#ember-app'\n\t});\n});"
};