module.exports = {
	prompts: [
		{
			text: 'What SObject should the trigger run on?',
			replace: 'sobject',
			required: true,
			default: 'Account'
		},
		{
			text: 'Status (Active or Inactive)',
			replace: 'status',
			required: false,
			default: 'Active'
		}
	],
	
	templateString: {
		"apiVersion": "{{apiVersion}}",
		"content": "trigger {{fullName}} on {{sobject}} (before insert) {}",
		"status": "{{status}}"
	}
};