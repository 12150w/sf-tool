module.exports = {
	prompts: [
		{
			text: 'Available in touch?',
			replace: 'touch',
			required: false,
			default: false
		},
		{
			text: 'Confirmation token required?',
			replace: 'token',
			required: false,
			default: false
		}
	],
	
	templateString: {
		"apiVersion": "{{apiVersion}}",
		"label": "{{fullName}}",
		"content": "<apex:page></apex:page>",
		"availableInTouch": "{{touch}}",
		"confirmationTokenRequired": "{{token}}"
	}
};