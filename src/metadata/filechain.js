/**
	@class FileChain
	@classdesc A class that models a file and all of its dependent files
	@param {Metadata} component An instance of a Metadata
	@param {Object} options An options hash
	@param {string} [dependencyFolder='dependencies'] - The name of the dependency folder in the project root folder
	@param {string[]} [baseOverrides=['content']] - The properties to always override when merging
*/
var Class = require('level2-base').Class,
	path = require('path'),
	fs = require('fs-extra'),
	Project = require('../base/project'),
	typesRegistry = require('./types/registry');

module.exports = Class.extend({
	
	// init sets the component and options
	init: function(component, options) {
		this.component = component;
		this.options = options || {};
		this.dependencyFolder = this.options.dependencyFolder || 'dependencies';
		this.baseOverrides = this.options.baseOverrides || ['content'];
	},
	
	/** Reads the metadata and it's dependent metadata chain and merges them into 1 object
		@memberof FileChain
		@instance
		@return {Promise | Object} An object representing the merged metadata
	*/
	merge: function() {
		var self = this;
		return this.getFiles(this.component).then(function(chain) {
			
			return self._mergeObjects(chain);
			
		});
	},
	
	/** Recursively creates an array of files that need to be merged; returns a promise
		@memberof FileChain
		@instance
		@return {Promise | Object[]} An array of metadata files
	*/
	getFiles: function(component, isDependencyChain) {
		// Add the file to the metadata
		var self = this,
			chain = [];
		
		// isDependencyChain defaults to false
		isDependencyChain = !!isDependencyChain;
		
		// Read the metadata file from the disk
		return component.read().then(function(rawData) {
			
			// Add the file to the chain
			if(!!rawData) {
				chain.unshift(rawData);
			}
			
			// Check for a dependencies folder; return the chain if none
			return self._getDependentProjects(component).fail(function() {
				throw chain;
			});
			
		}).then(function(dependentProjects) {
			// Recursively get further dependency files
			var dependentChainPromises = [],
				ReadType = typesRegistry.forName(self.component.contentType || self.component.typeName),
				dependentMetadata;
			
			// Create a new metadata instance for the dependent project and get its file chain
			for(var i=0; i<dependentProjects.length; i++) {
				dependentMetadata = new ReadType(self.component.fullName, dependentProjects[i]);
				dependentChainPromises.push(self.getFiles(dependentMetadata, true));
			}
			
			return q.all(dependentChainPromises);
			
		}).then(function(dependentChains) {
			// Filter out any empty chains
			dependentChains = dependentChains.filter(function(dependentChain) {
				return dependentChain.length > 0;
			});
			
			// Stop and throw an error if there are 2 or more dependencies for the same file on the same level
			if(dependentChains.length > 1) {
				throw new Error('2 Dependencies modify the metadata on the same level');
			}
			
			// If there are any dependent chains, link them to the existing chain
			if(!!dependentChains[0]) {
				chain = dependentChains[0].concat(chain);
			}
			
			// Check to see if the chain is empty
			if(chain.length === 0 && !isDependencyChain) {
				throw new Error('The file "' + self.component.fullName + '" does not exist');
			}
			
			return chain;
			
		}).fail(function(reason) {
			// If there really is an error, propogate it; otherwise return the result
			if(reason instanceof Error) {
				throw reason;
			} else {
				return reason;
			}
		});
	},
	
	/** Merges a list of objects into one
		@memberof FileChain
		@instance
		@private
		@return {Object} An object whose properties have been merged
		@param {Object} base The destination object
		@param {Object[]} changeList An array of objects to merge into the base object
	*/
	_mergeObjects: function(base, changeList) {
		if(arguments.length === 1) {
			changeList = base;
			base = {};
		}
		
		if(changeList.length === 0) {
			return base;
		}
		
		// Pull the first change in out of the array
		var change = changeList.shift();
		
		// Recursively merge the the rest of the array
		return this._mergeObjects(
			this._mergeObjectProperties(base, change),
			changeList
		);
	},
	
	/** Merges the values of two arrays containing only objects
		@memberof FileChain
		@instance
		@private
		@return {Object[]} An object array that has been merged with another array
		@param {Object[]} baseArray The destination array
		@param {Object[]} mergeArray The array to merge into the baseArray
		@param {string} keyName The property name of the objects in the array used for comparison
		@param {boolean} override Forces the objects in the array to be merged
	*/
	_mergeObjectArrays: function(baseArray, mergeArray, keyName, override) {
		// If there is no property key name, return the base array
		if(!keyName) {
			return baseArray;
		}
		var subMetadataMap = {}, key;
		
		// Map the base sub-metadata by the fullName property
		for(var i=0; i<baseArray.length; i++) {
			key = baseArray[i][keyName];
			if(!key) continue;
			
			subMetadataMap[key] = baseArray[i];
		}
		
		// Map the merge sub-metadata by the dynamic named property
		for(var i=0; i<mergeArray.length; i++) {
			key = mergeArray[i][keyName];
			if(!key) continue;
			
			// If the sub-metadata is already mapped (or override is false), merge it;
			// Otherwise just add it to the map
			if(!!subMetadataMap[key] && !!override) {
				subMetadataMap[key] = this._mergeObjectProperties(
					subMetadataMap[key], mergeArray[i]
				);
			} else {
				subMetadataMap[key] = mergeArray[i];
			}
		}
		
		// Turn the object into an array
		return Object.keys(subMetadataMap)
			.map(function(key) { return subMetadataMap[key]; });
	},
	
	/** Recursively merges 2 objects
		@memberof FileChain
		@instance
		@private
		@return {Object} The merged object
		@param {Object} base The destination object
		@param {Object} mergeObject The object to merge into the base object
		@param {boolean} override Forces properties to be overridden in the base object by the merge object
	*/
	_mergeObjectProperties: function(base, mergeObject, override) {
		// defaults are blank objects and override base object properties
		base = base || {};
		mergeObject = mergeObject || {};
		override = (override !== null && override !== undefined) ? override : true;
		
		var properties = Object.keys(mergeObject),
			baseValue, mergeValue, finalValue,
			baseType, mergeType;
		for(var i=0; i<properties.length; i++) {
			if(!mergeObject.hasOwnProperty(properties[i])) continue;
			
			baseValue = base[properties[i]];
			mergeValue = mergeObject[properties[i]];
			
			baseType = Object.prototype.toString.call(baseValue);
			mergeType = Object.prototype.toString.call(mergeValue);
			
			// If the base value exists and we don't want to override, re-use the base value
			if(!override && baseValue !== undefined && baseValue !== null) {
				continue;
			}
			
			// If the values are both objects, merge the nested objects
			// If the values are both arrays of sub-metadata, merge the arrays
			// Otherwise, use the change value
			if(baseType === '[object Object]' && mergeType === '[object Object]') {
				finalValue = this._mergeObjectProperties(baseValue, mergeValue, override);
			} else if(
				baseType === '[object Array]' && mergeType === '[object Array]'
				&& this.subTypes && !!this.subTypes[properties[i]]
			) {
				finalValue = this._mergeObjectArrays(baseValue, mergeValue, 'fullName', override);
			} else {
				finalValue = mergeValue;
			}
			
			base[properties[i]] = finalValue;
		}
		
		return base;
	},
	
	/** Looks for projects in the dependency folder
		@memberof FileChain
		@instance
		@private
		@return {Promise | Project} A project in the dependency folder
	*/
	_getDependentProjects: function(component) {
		var dependencyPath = path.join(component._project.path(), this.dependencyFolder),
			projectPaths = [];
		
		// Read all the files in the given folder path
		return q.nfcall(fs.readdir, dependencyPath).then(function(filenames) {
			// Check each file in the dependency folder to see if it is a directory
			var statPromises = [];
			for(var i=0; i<filenames.length; i++) {
				projectPaths.push(path.join(dependencyPath, filenames[i]));
				
				statPromises.push(
					q.nfcall(fs.stat, projectPaths[i])
				);
			}
			
			return q.all(statPromises);
			
		}).then(function(stats) {
			var projects = [];
			for(var i=0; i<stats.length; i++) {
				if(stats[i].isDirectory()) {
					projects.push(new Project(projectPaths[i]));
				}
			}
			
			return projects;
			
		});
		
	}
	
});