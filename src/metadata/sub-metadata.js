/**
	@class SubMetadata
	@classdesc A metadata that is a part of a compound metadata
	@extends Metadata
	@param {string} fullName The name of the Metadata type
	@param {Project} project An instance of a SF Tool project
	@param {CompountMetadata} parent An instance of CompountMetadata
*/
var Metadata = require('./metadata'),
	Project = require('../base/project'),
	NotImplementedError = require('../base/errors').NotImplementedError,
	q = require('q'),
	fs = require('fs-extra'),
	typesRegistry = require('./types/registry'),
	xml = require('../base/xml'),
	path = require('path');

module.exports = Metadata.extend({
	folder: null,
	extension: null,
	
	// init constructs a new SubMetadata object
	init: function(fullName, project, parent) {
		var nameMatch = /^([^\.]+)\.([^\.]+)$/.exec(fullName),
			parentFullName = nameMatch && nameMatch[1] || this.parentTypeName,
			subFullName = nameMatch && nameMatch[2] || fullName;
		
		this._super(subFullName, project);
		this.fullName = fullName;
		this._subFullName = subFullName;
		
		if(parent === null || parent === undefined) {
			var ParentType = typesRegistry.forName(this.parentTypeName);
			parent = new ParentType(parentFullName, project);
		}
		
		this._parent = parent;
	},
	
	/** Throws an error because SubMetadata instances cannot be compressed by themselves
		@memberof SubMetadata
		@instance
		@return {null}
	*/
	compress: function() {
		throw new NotImplementedError('compress() does not work for SubMetadata, it should be called on the parent metadata');
	},
	
	/** Pulls the sub-metdata from the zip file
		@memberof SubMetadata
		@instance
		@return {Promise | Object} Object representation of the sub-metadata
		@param {AdmZip} zip An instance of node adm-zip
	*/
	extract: function(zip) {
		var self = this,
			parentPkgPath = this._parent.getExtractPath();
		
		// Find the parent key
		var parentKey = null;
		for(var subKey in this._parent.subTypes) {
			if(this._parent.subTypes[subKey] === this.typeName) {
				parentKey = subKey;
				break;
			}
		}
		
		// Read the xml
		return this._extractFile(zip, parentPkgPath).then(function(parentSrc) {
			// Parse the xml
			if(parentSrc == null) {
				throw new Error('Package did not have "' + parentPkgPath + '"');
			}
			return xml.parse(parentSrc.toString('utf8'));
			
		}).then(function(parentData) {
			// Extract from the parent data
			if(parentData[parentKey] == null) {
				throw new Error('Sub metadata key "' + parentKey + '" not found on parent data');
			}
			var parentList = parentData[parentKey];
			if(Object.prototype.toString.call(parentList) !== '[object Array]') {
				parentList = [parentList];
			}
			
			for(var i=0; i<parentList.length; i++) {
				if(parentList[i].fullName === self._subFullName) {
					return parentList[i];
				}
			}
			
			throw new Error('Sub metadata ' + self.typeName + ' "' + self.fullName + '" not found in parent');
		});
		throw new Error('extract() does not work for SubMetadata, it should be called on the parent metadata');
	},
	
	/** Gets the relative path to the sub-metadata folder within the compound metadata folder
		@memberof SubMetadata
		@instance
		@return {string} The name of the folder
	*/
	getFolderName: function() {
		return path.join(this._parent.getFolderName(), this.typeName);
	},
	
	/** Gets the full name of the sub-metadata type
		@memberof SubMetadata
		@instance
		@return {string} The fullname (<parent fullname>.<child fullname>)
	*/
	getPackagedName: function() {
		return this._parent.getFullName() + '.' + this._subFullName;
	},
	
	/** Gets the file path to this metadata resource on the disk
		@memberof SubMetadata
		@instance
		@return {string} The system path to the metadata file
	*/
	getFilePath: function() {
		return path.join(this.getFolderPath(), this._subFullName + '.json');
	},
	
});