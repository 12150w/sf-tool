/**
	@class CompoundMetadata
	@classdesc A metadata that contains sub-metadata
	@extends Metadata
	@param {string} fullName The name of the Metadata type
	@param {Project} project An instance of a SF Tool project
*/
var Metadata = require('./metadata'),
	typesRegistry = require('./types/registry'),
	FileChain = require('./filechain'),
	path = require('path'),
	q = require('q'),
	fs = require('fs-extra'),
	xml = require('../base/xml');

module.exports = Metadata.extend({
	
	// init constructs a new CompoundMetadata object
	init: function() {
		this._super.apply(this, arguments);
		this._restrictedSubs = null;
	},
	
	/** reads the metadata base file and any sub-metadata files from the disk
		@memberof CompoundMetadata
		@instance
		@return {Promise | Object} Object repesentation of the metadata
	*/
	read: function() {
		var baseMetadata = {};
		
		this._rootData = false;
		
		// Call the inherited read() first to get the base metadata
		var self = this;
		return this._super().then(function(metadata) {
			if(metadata === null) {
				self._rootData = true;
			} else {
				baseMetadata = metadata;
			}
			
			// Read in the sub metadata
			var subReadPromises = [], metadataTypeName;
			for(var key in self.subTypes) {
				metadataTypeName = self.subTypes[key];
				if(
					self._restrictedSubs != null
					&& self._restrictedSubs[metadataTypeName] == null
				) {
					continue;
				}
				
				// Read the sub-metadata for the given folder
				subReadPromises.push(self._readSubMetadata(
					self.getFolderPath(), key, metadataTypeName
				));
			}
			
			return q.all(subReadPromises);
			
		}).then(function(subMetadataList) {
			// Add each sub-metadata object to the base metadata object
			subMetadataList.forEach(function(subMetadata) {
				if(subMetadata == null) return;
				baseMetadata[subMetadata.key] = subMetadata.data;
			});
			
			// If no sub-metadata was added to the base metadata
			// and the base metadata was null, return null
			return Object.keys(baseMetadata).length > 0 ? baseMetadata : null;
		});
		
	},
	
	/** Saves the metadata and sub-metadata files to the disk
		@memberof CompoundMetadata
		@instance
		@return {Promise} A promise that resolves when the file has been written
		@param {Object} data Object representation of the metadata
	*/
	write: function(data) {
		// Remove xmlns
		delete data['$'];
		
		// Remove the sub-metadata
		var subData = {};
		for(var subKey in this.subTypes) {
			subData[subKey] = data[subKey];
			if(subData[subKey] == null) {
				subData[subKey] = [];
			} else if(Object.prototype.toString.call(subData[subKey]) !== '[object Array]') {
				subData[subKey] = [subData[subKey]];
			}
			
			delete data[subKey];
		}
		
		// Write the output to the disk
		var self = this;
		return q.nfcall(
			fs.outputFile,
			this.getFilePath(),
			JSON.stringify(data, null, '	'),
			{ encoding: 'utf8' }
		).then(function() {
			var writePromises = [];
			
			for(var subKey in self.subTypes) {
				if(subData[subKey] == null) continue;
				var SubType = typesRegistry.forName(self.subTypes[subKey]);
				
				for(var i=0; i<subData[subKey].length; i++) {
					var subMeta = new SubType(subData[subKey][i].fullName, self._project, self);
					
					writePromises.push(subMeta.write(subData[subKey][i]));
				}
			}
			
			return q.all(writePromises);
		});
	},
	
	/** Reads the metadata/sub-metadata and its dependencies, merges them and then adds them to a zip file
		@memberof CompoundMetadata
		@instance
		@return {null}
		@param {AdmZip} zip An instance of node adm-zip
	*/
	compress: function(zip) {
		this._rootData = false;
		
		var self = this;
		return new FileChain(this).merge().then(function(data) {
			if(self._rootData === true) {
				data = { root: data };
			}
			
			var metaXml = xml.build(self.typeName, data),
				zipPath = self.getCompressPath();
			
			zip.file(zipPath, new Buffer(metaXml, 'utf8'));
		});
	},
	
	/** Adds a sub-metadata component to this CompoundMetadata instance
		@memberof CompoundMetadata
		@instance
		@return {null}
		@param {SubMetadata} component An instance of SubMetadata
	*/
	addSubComponent: function(component) {
		// Parse the sub-metadata name from the complete name
		var subMatch = /^[^\.]+\.([^\.]+)$/.exec(component.getPackagedName());
		if(subMatch == null) {
			throw new Error('Invalid sub-metadata name "' + component.fullName + '"');
		}
		var subName = subMatch[1];
		
		if(this._restrictedSubs == null) {
			this._restrictedSubs = {};
		}
		
		if(this._restrictedSubs[component.typeName] == null) {
			this._restrictedSubs[component.typeName] = {};
		}
		
		this._restrictedSubs[component.typeName][subName] = component;
	},
	
	/** Gets the path to the metadata type's sub-folder
		@memberof CompoundMetadata
		@instance
		@return {string} The relative path of the metadata folder and type sub-folder
	*/
	getFolderName: function() {
		return path.join(this.folder, this.fullName);
	},
	
	/** Reads the sub-metadata files from their folders
		@memberof CompoundMetadata
		@private
		@return {Promise | SubMetadataMap} An object with key and data properties
	*/
	_readSubMetadata: function(subFolderPath, subTypeKey, typeName) {
		// List the sub files
		var subMetadataPath = path.join(subFolderPath, typeName),
			SubType = typesRegistry.forName(typeName);
		
		// Read the contents of the sub-metadata folder
		var self = this;
		return q.nfcall(fs.readdir, subMetadataPath).then(function(filenames) {
			var readPromises = [];
			
			// Create a sub-metadata instance for each file and read the contents
			filenames.forEach(function(fileName) {
				var subMetadataName = fileName.replace(/\.json$/, '');
				if(
					self._restrictedSubs != null
					&& self._restrictedSubs[typeName][subMetadataName] == null
				) {
					return;
				}
				
				readPromises.push(
					new SubType(subMetadataName, self._project, self).read()
				);
			});
			
			return q.all(readPromises);
			
		}).then(function(subMetadata) {
			// Return the key for the sub-metadata type and the submetadata objects
			return {
				key: subTypeKey,
				data: subMetadata
			};
		}).fail(function(reason) {
			// If the folder doesn't exist, ignore the error
			if(reason.code !== 'ENOENT') {
				throw reason;
			}
		});
	}
	
});