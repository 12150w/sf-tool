/**
	@module contentRegistry
	@desc Provides access to all available types given a type name
*/
var q = require('q'),
	glob = require('glob'),
	path = require('path');

var contentTypes = module.exports = {
	_registry: {
		'CONTENTMETADATA': require('./content')
	},
	
	/** Gets the content type given a name
		@param {string} name - The name of the content type
		@return {Content} The content class
	*/
	forName: function(name) {
		if(name === null) {
			throw new Error('Unsupported content type: null');
		}
		
		// Check the registry
		var typeDef = this._registry[name.toUpperCase()];
		if(typeDef == null) {
			throw new Error('Unsupported content type: "' + name + '"');
		}
		
		return typeDef;
	}
};

// Populate the registry
glob.sync('*.js', {
	cwd: path.join(__dirname, 'types')
}).forEach(function(partialPath) {
	var typeDef = require('./types/' + partialPath);
	contentTypes._registry[typeDef.prototype.typeName.toUpperCase()] = typeDef;
});