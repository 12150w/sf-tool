/**
	@class Content
	@classdesc The content file associated with a content metadata
	@param {ContentMetadata} component - The content metadata this content is for
*/
var Class = require('level2-base').Class,
	fs = require('fs-extra'),
	path = require('path'),
	q = require('q');

module.exports = Class.extend({
	
	/** The name of the content type
		@memberof Content
		@instance
	*/
	typeName: null,
	
	// construct the content given a metadata component
	init: function(component) {
		this.component = component;
	},
	
	/** Returns the file path to this metadata's content on the disk
		@memberof Content
		@instance
		@virtual
		@return {string} The filepath to the content
	*/
	getFilePath: function() {
		return path.join(this.component.getFolderPath(), this.component.getPackagedName());
	},
	
	/** Returns the path to where the content is stored in a deploy package
		@memberof Content
		@instance
		@virtual
		@return {string} The path to the content
	*/
	getCompressPath: function() {
		return this.component.getCompressPath().replace('-meta.xml', '');
	},
	
	/** Returns the path to where the content is stored in a retrieve package
		@memberof Content
		@instance
		@virtual
		@return {string} The path to the content
	*/
	getExtractPath: function() {
		return 'unpackaged/' + this.getCompressPath();
	},
	
	/** Reads the content file from the disk
		@memberof Content
		@instance
		@virtual
		@return {Promise | Object} The content file
	*/
	read: function() {
		return q.nfcall(fs.readFile, this.getFilePath());
	},
	
	/** Writes the content file to the disk
		@memberof Content
		@instance
		@virtual
		@param {Content} content - The content to store
		@return {Promise}
	*/
	write: function(content) {
		return q.nfcall(fs.outputFile, this.getFilePath(), content);
	},
	
	/** Deletes the content file for the component
		@memberof Content
		@instance
		@virtual
		@return {Promise}
	*/
	remove: function() {
		return q.nfcall(fs.remove, this.getFilePath());
	},
	
	/** Pulls the content from the package XML
		@memberof Content
		@instance
		@virtual
		@return {Promise | Object} The content file
	*/
	extract: function(zip) {
		return this.component._extractFile(zip, this.getExtractPath());
	}
	
});