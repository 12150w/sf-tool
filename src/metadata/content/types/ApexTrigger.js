/**
	@class ApexTriggerContent
	@classdesc ApexTrigger metadata content
	@extends Content
*/
var Content = require('../content');

module.exports = Content.extend({
	typeName: 'ApexTrigger'
});