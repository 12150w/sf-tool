/**
	@class ApexPageContent
	@classdesc ApexPage metadata content
	@extends Content
*/
var Content = require('../content');

module.exports = Content.extend({
	typeName: 'ApexPage'
});