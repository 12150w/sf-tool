/**
	@class SFZipFileContent
	@classdesc Zip folder metadata content
	@extends Content
*/
var Content = require('../content'),
	AdmZip = require('adm-zip'),
	JSZip = require('jszip'),
	q = require('q'),
	fs = require('fs-extra'),
	glob = require('glob'),
	path = require('path');

module.exports = Content.extend({
	typeName: 'SFZipFile',
	
	/** Reads the zip file from the hard disk
		@memberof SFZipFileContent
		@instance
		@return {Promise | Object} The zip file
	*/
	read: function() {
		var zipFolderPath = this.getFilePath(),
			zip = new JSZip();
		
		return q.nfcall(glob, '**/**.*', { cwd: zipFolderPath }).then(function(files) {
			var readPromises = [];
			
			files.forEach(function(file) {
				var filePath = path.join(zipFolderPath, file);
				
				var readPromise = q.nfcall(fs.stat, filePath).then(function(fileStats) {
					if(fileStats.isDirectory() === true) {
						return;
					}
					
					return q.nfcall(fs.readFile, filePath).then(function(itemContents) {
						zip.file(file, itemContents, {
							createFolders: true,
							binary: true
						});
					});
				});
				
				readPromises.push(readPromise);
			});
			
			return q.all(readPromises);
			
		}).then(function() {
			return zip.generateAsync({
				compression: 'DEFLATE',
				type: 'nodebuffer'
			});
			
		});
	},
	
	/** Writes to the zip file on the disk
		@memberof SFZipFileContent
		@instance
		@param {Content} content - The content to store
		@param {Object} [template] - The template for the zip file
		@return {Promise}
	*/
	write: function(content, template) {
		// If there is a not a template, extract the content of the zip file to the disk;
		// If there is, create a new folder on the disk
		if(!template) {
			if(!Buffer.isBuffer(content)) {
				content = Buffer.from(content);
			}
			var zip = new AdmZip(content);
			
			return q(zip.extractAllTo(this.getFilePath(), true));
			
		} else {
			// Add a blank css file to a new folder
			return q.nfcall(fs.outputFile, path.join(this.getFilePath(), 'styles.css'), content);
		}
	},
	
	/** Returns the file path to this metadata's content on the disk
		@memberof SFZipFileContent
		@instance
		@return {string} The filepath to the content
	*/
	getFilePath: function() {
		return path.join(this.component.getFolderPath(), this.component.getFullName());
	}
	
});