/**
	@class ApexClassContent
	@classdesc ApexClass metadata content
	@extends Content
*/
var Content = require('../content');

module.exports = Content.extend({
	typeName: 'ApexClass'
});