/**
	@class EmailTemplateContent
	@classdesc EmailTemplate metadata content
	@extends Content
*/
var Content = require('../content');

module.exports = Content.extend({
	typeName: 'EmailTemplate'
});