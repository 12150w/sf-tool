/**
	@class StaticResourceContent
	@classdesc StaticResource metadata content
	@extends Content
*/
var Content = require('../content');

module.exports = Content.extend({
	typeName: 'StaticResource'
});