/**
	@class SFEmberAppContent
	@classdesc Ember application metadata content
	@extends Content
	@param {ContentMetadata} component - The content metadata this content is for
	@param {string} version - The ember application version
*/
var Content = require('../content'),
	q = require('q'),
	fs = require('fs-extra'),
	EmberCompiler = require('level2-ember').Compiler,
	path = require('path'),
	uglify = require('uglify-js').minify,
	NotImplementedError = require('../../../base/errors').NotImplementedError;

module.exports = Content.extend({
	typeName: 'SFEmberApp',
	
	syncDisabled: false,
	emberVersion: null,
	
	init: function(component, version) {
		this._super.apply(this, arguments);
		this.emberVersion = (version || null);
	},
	
	/** Reads the ember application and compiles into a single source file
		@memberof SFEmberAppContent
		@instance
		@return {Promise | Object} An ember application JavaScript file
	*/
	read: function() {
		var appPath = this.getFilePath(),
			dependencyPath = path.join(appPath, '..', 'dependencies'),
			templateCompilerPath = path.join(appPath, '..', 'ember-template-compiler.js'),
			minify = this.isProduction;
		
		return q.nfcall(fs.stat, templateCompilerPath).then(function(compilerStats) {
			if(!compilerStats.isFile()) {
				templateCompilerPath = null;
			}
		}, function(err) {
			if(err.code === 'ENOENT') {
				templateCompilerPath = null;
			} else {
				throw err;
			}
			
		}).then(function() {
			// Run the compiler
			var compiler = new EmberCompiler({
					debug: minify !== true,
					dependencies: dependencyPath,
					version: this.emberVersion || null,
					templateCompiler: templateCompilerPath
				}),
				compileDefer = q.defer();
			
			// Handle data
			var outBuffer = [];
			compiler.on('data', function(chunk) {
				outBuffer.push(chunk);
			});
			compiler.on('end', function() {
				if(hadError === true) {
					return;
				}
				compileDefer.resolve(Buffer.concat(outBuffer));
			});
			
			// Handle errors
			var hadError = false;
			compiler.on('error', function(err) {
				if(hadError === true) {
					return;
				}
				hadError = true;
				compileDefer.reject(err);
			});
			
			// Start compiling
			compiler.compileProject(appPath);
			
			return compileDefer.promise;
			
		}).then(function(compiled) {
			// Minify (if needed)
			if(minify !== true) {
				return compiled;
			}
			
			var minString = uglify(compiled.toString('utf8'), { fromString: true });
			
			return new Buffer(minString.code, 'utf8');
		});
	},
	
	/** Creates a new application from a template
		@memberof SFEmberAppContent
		@instance
		@param {Content} content
		@param {Object} [template] - A template to create a new ember application from
		@return {Promise} If only content is passed in, then an error is thrown (normal write functionality)
	*/
	write: function(content, template) {
		// If not creating a new App, throw an error
		if(!template) {
			throw new NotImplementedError('SFEmberApp.write() is not allowed');
		}
		
		// Create the ember app.js file and it's app folder in /ember
		return q.nfcall(fs.outputFile, path.join(this.getFilePath(), 'app.js'), template.appTemplateString || '');
	},
	
	/** Deletes the ember app folder for the component
		@memberof SFEmberAppContent
		@instance
		@return {Promise}
	*/
	remove: function() {
		return q.nfcall(fs.remove, this.getFilePath());
	},
	
	/** Returns an empty buffer
		@memberof SFEmberAppContent
		@instance
		@return {Buffer} An empty string buffer
	*/
	extract: function() {
		return new Buffer('');
		
		// Make the static meta content
		var meta = {
				cacheControl: "Public",
				contentType: "application/javascript"
			},
			metaBody = JSON.stringify(meta, null, '   ');
	},
	
	/** Returns the path to the ember project folder
		@memberof SFEmberAppContent
		@instance
		@return {string} The path to the folder
	*/
	getFilePath: function() {
		return path.join(this.component._project.path(), 'ember', this.component.getFullName());
	}
	
});