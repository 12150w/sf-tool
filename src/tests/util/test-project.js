/* TestProject
 *     Extends Project providing a clean way to create
 *     testing projects
 */
var Project = require('../../project'),
	os = require('os'),
	path = require('path'),
	q = require('q');

var TestProject = Project.extend({
	init: function() {
		var testFolder = path.join(os.tmpdir(), 'sf-test-project-' + (TestProject._instances.length + 1));
		
		this._super(testFolder);
		TestProject._instances.push(this);
	}
});

// Handling test project deletion
TestProject._instances = [];

// Inject an after handler if running in Mocha
if(typeof(after) === 'function') {
	after('Delete all TestProject folders', function(done) {
		var destroyPromises = [];
		for(var i=0; i<TestProject._instances.length; i++) {
			destroyPromises.push(TestProject._instances[i].destroy());
		}
		
		q.all(destroyPromises).then(function() {done();}, done).done();
	});
}

module.exports = TestProject;
