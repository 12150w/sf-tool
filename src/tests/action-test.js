/* Tests the base Action class
 */
var Action = require('../action'),
	errors = require('../base/errors'),
	assert = require('assert');

describe('Action', function() {
	var testAction;
	beforeEach(function() {
		testAction = new Action();
	});
	
	describe('#run', function() {
		it('Throws not implemented', function(done) {
			testAction.run().then(function() {
				done(new Error('NotImplementedError was not thrown'));
			}, function(err) {
				if(err instanceof errors.NotImplementedError) done();
				else done(new Error('Expecting a NotImplementedError'));
			});
		});
	});
});
