/* Tests the version action
 */
var sf = require('../..'),
	assert = require('assert');

describe('Version Action', function() {
	describe('Data', function() {
		it('Contains version data', function(done) {
			sf.actions.version.run().then(function(data) {
				assert.ok(!!data.version);
				done();
			}).done();
		});
		
		it('Contains description data', function(done) {
			sf.actions.version.run().then(function(data) {
				assert.ok(!!data.description);
				done();
			}).done();
		});
		
		it('Contains website data', function(done) {
			sf.actions.version.run().then(function(data) {
				assert.ok(!!data.website);
				done();
			}).done();
		});
	});
});