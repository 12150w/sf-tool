/* Tests the Project class
 */
var Project = require('../project'),
	TestProject = require('./util/test-project'),
	assert = require('assert'),
	fs = require('fs-extra'),
	os = require('os'),
	path = require('path'),
	q = require('q');

describe('Project', function() {
	var testProject = new TestProject();
	
	describe('#destroy', function() {
		it('Removes the project folder and contents', function(done) {
			var deleteProject = new Project(path.join(os.tmpdir(), 'test-destroy-project')),
				dummyContentPath = path.join(deleteProject.path(), 'test.txt');
			
			// Delete the testing folder if it exists
			q.nfcall(fs.remove, deleteProject.path()).fail(function(err) {
				if(err.code !== 'ENOENT') throw err;
				
			}).then(function() {
				// Create the contents
				deleteProject.initialize()
				
			}).then(function() {
				// Add some contents
				return q.nfcall(fs.writeFile, dummyContentPath, 'test', {encoding: 'utf8'});
				
			}).then(function() {
				// Destroy the contents
				return deleteProject.destroy();
				
			}).then(function() {
				// Verify the contents are gone
				return q.nfcall(fs.stat, deleteProject.path()).then(function() {
					assert.ok(false, 'The project folder should be deleted');
				}, function(err) {
					if(err.code !== 'ENOENT') throw err;
				});
				
			}).then(function() {done();}, done).done();
		});
	});
	
	
	describe('#path', function() {
		it('Returns the project content path', function(done) {
			// Verify content folder exists
			testProject.initialize().then(function() {
				// Stat the content folder
				return q.nfcall(fs.stat, testProject.path());
				
			}).then(function(contentStats) {
				assert.ok(contentStats.isDirectory() === true);
				
			}).then(function() {done();}, done).done();
		});
	});
	
});