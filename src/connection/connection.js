/**
	@class Connection
	@classdesc A single authenticated connection to SalesForce. Provides API access
	@param {Project} project - A project instance this connection is for
	@param {string} connectionName - The name to refer to the connection by
*/
var Class = require('level2-base').Class,
	ConnectionResponse = require('./response'),
	UIError = require('../base/errors').UIError,
	HTTPError = require('../base/errors').HTTPError,
	MetaAPI = require('../api/meta'),
	ApexAPI = require('../api/apex'),
	oauth = require('../base/oauth'),
	http = require('http'),
	https = require('https'),
	url = require('url'),
	path = require('path'),
	q = require('q');

module.exports = Class.extend({
	
	// constructs given a project and name
	init: function(project, connectionName) {
		this.project = project;
		
		this._name = connectionName || 'default';
		this._updatingToken = false;
		this.apiVersion = '43.0';
		
		// Create SOAP APIs
		this.meta = new MetaAPI(this);
		this.apex = new ApexAPI(this);
	},
	
	/** Saves the OAuth information about the connection in the project settings
		@memberof Connection
		@instance
		@param {Object} data - The OAuth information
		@return {Promise}
	*/
	storeAuthorization: function(data) {
		var self = this;
		data = data || {};
		
		// Verify required attributes
		if(data.refresh_token == null) throw new Error('Authorization does not contain refresh_token');
		if(data.instance_url == null) throw new Error('Authorization does not contain instance_url');
		if(data.id == null) throw new Error('Authorization does not contain id');
		if(data.login_server == null) throw new Error('Authorization does not contain login_server');
		if(data.access_token == null) throw new Error('Authorization does not contain access_token');
		
		// Persist the data
		return self.project.settings.set('connections.' + self._name, {
			refresh_token: data.refresh_token,
			instance_url: data.instance_url,
			id: data.id,
			login_server: data.login_server,
			limited: data.scope != null ? data.scope.indexOf('full') < 0 : data._isReloadAsLimited
			
		}).then(function() {
			// Persist the session token
			return self.project.cache.set('sessions.' + self._name, data.access_token);
			
		}).then(function() {
			// Load identity
			return self.sfRequest(data.id, 'GET').then(function(res) {
				return self.project.settings.set('connections.' + self._name + '.urls', res.json().urls);
			});
			
		}).then(function() {
			// Load the server information
			self.meta.reset();
			
			return self.meta.run('describeMetadata', {
				apiVersion: parseInt(self.apiVersion, 10)
				
			}).then(function(describeData) {
				var info = describeData.res;
				return self.project.settings.set('connections.' + self._name + '.info', {
					namespace: info.organizationNamespace || null
				});
				
			});
			
		});
	},
	
	/** Sends a HTTP request to the connection
		@memberof Connection
		@instance
		@param {string} fullUrl - The URL to send the request to
		@param {string} method - The HTTP method to use
		@param {Object} options - An options hash
		@return {Promise | ConnectionResponse} The HTTP response
	*/
	request: function(fullUrl, method, options) {
		var self = this;
		options = options || {};
		
		return q().then(function() {
			// Prepare the request
			var urlData = url.parse(fullUrl);
			
			// Inject params to URL
			var urlPath = urlData.pathname;
			if(options.params != null) {
				var injectedParams = [];
				
				for(var paramKey in options.params) {
					if(!options.params.hasOwnProperty(paramKey)) continue;
					injectedParams.push(
						encodeURIComponent(paramKey) + '=' + encodeURIComponent(options.params[paramKey])
					);
				}
				
				if(injectedParams.length > 0) {
					urlPath += '?' + injectedParams.join('&');
				}
			}
			
			// Check for JSON header
			var headers = options.headers || {};
			if(typeof(options.body) !== 'string' && options.body instanceof Buffer === false) {
				if(options.body != null) {
					headers['Content-Type'] = 'application/json';
				}
			}
			
			return {
				protocol: urlData.protocol,
				hostname: urlData.hostname,
				port: urlData.port,
				path: urlPath,
				method: method,
				headers: headers
			};
			
		}).then(function(httpOptions) {
			// Send the request
			var httpRequester = http;
			if(httpOptions.protocol === 'https:') httpRequester = https;
			var httpRes = httpRequester.request(httpOptions);
			
			// Send the body if available
			if(!!options.body) {
				if(typeof options.body === 'string') {
					httpRes.write(options.body);
				} else if(options.body instanceof Buffer) {
					httpRes.write(options.body);
				} else {
					httpRes.write(JSON.stringify(options.body));
				}
			}
			
			// End the request
			var reqDefer = q.defer();
			httpRes.on('response', function(incoming) {
				reqDefer.resolve(incoming);
			});
			httpRes.on('error', function(err) {
				reqDefer.reject(err);
			});
			
			httpRes.end();
			return reqDefer.promise;
			
		}).then(function(incoming) {
			// Process the response
			var resDefer = q.defer(),
				resBuffer = [];
			
			// Handle data
			incoming.on('data', function(chunk) {
				resBuffer.push(chunk);
			});
			
			// Handle error
			incoming.on('error', function(err) {
				resDefer.reject(err);
			});
			
			// Handle end
			incoming.on('end', function() {
				resDefer.resolve(new ConnectionResponse(
					incoming,
					resBuffer.length > 0 ? new Buffer.concat(resBuffer) : null
				));
			});
			
			return resDefer.promise;
			
		}).then(function(res) {
			// Check for HTTP error
			if(res.statusCode >= 400) {
				throw new HTTPError(res);
			}
			
			return res;
		});
	},
	
	/** Sends a HTTP request to the connection
		@memberof Connection
		@instance
		@param {string} endpoint - The endpoint to send the request to
		@param {string} method - The HTTP method to use
		@param {Object} options - An options hash
		@return {Promise | ConnectionResponse} The HTTP response
	*/
	sfRequest: function(endpoint, method, options) {
		var self = this;
		
		return this.loadConfig().then(function(auth) {
			options = options || {};
			var endpointData = url.parse(endpoint);
			
			// Create the full URL
			var sfUrl = endpoint;
			if(endpointData.hostname == null) {
				sfUrl = auth.instance_url;
				if(endpoint.length > 0 && endpoint.substring(0, 1) !== '/') {
					sfUrl += '/';
				}
				sfUrl += endpoint;
			}
			
			// Add authorization
			options.headers = options.headers || {};
			options.headers['Authorization'] = 'Bearer ' + auth.access_token;
			
			return self.request(sfUrl, method, options);
			
		}).fail(function(err) {
			// Check for refresh auth
			if(err instanceof HTTPError !== true) throw err;
			
			if(err.res.statusCode === 403 || err.res.statusCode === 401) {
				if(self._updatingToken === false) {
					self._updatingToken = true;
					return self.reloadToken().then(function() {
						return self.sfRequest(endpoint, method, options);
					}).then(function(res) {
						self._updatingToken = false;
						return res;
					});
				} else {
					throw err;
				}
			} else {
				throw err;
			}
			
		});
	},
	
	/** Loads the OAuth properties from the project settings for this connection
		@memberof Connection
		@instance
		@return {Promise | Object} The OAuth connection settings
	*/
	loadConfig: function() {
		var self = this,
			dataCopy;
		
		return this.project.settings.get('connections.' + this._name).then(function(config) {
			if(config == null) {
				throw new UIError('Invalid connection "' + self._name + '"');
			}
			
			dataCopy = JSON.parse(JSON.stringify(config));
			return self.project.cache.get('sessions.' + self._name);
			
		}).then(function(currentToken) {
			dataCopy.access_token = currentToken;
			return dataCopy;
			
		});
	},
	
	/** Returns the name of the connection
		@memberof Connection
		@instance
		@return {string} The name of the connection
	*/
	getName: function() {
		return this._name;
	},
	
	/** Reload the access token using the refresh token
		@memberof Connection
		@instance
		@return {Promise}
	*/
	reloadToken: function() {
		var self = this,
			storeBackLimited = false,
			auth;
		
		return self.loadConfig().then(function(readConfig) {
			// Request a new access token
			auth = readConfig;
			var requestUrl = 'https://' + auth.login_server + '/services/oauth2/token';
			
			var requestBody = 'grant_type=refresh_token';
			requestBody += '&refresh_token=' + encodeURIComponent(auth.refresh_token);
			if(readConfig.limited === true) {
				storeBackLimited = true;
				requestBody += '&client_id=' + encodeURIComponent(oauth.RESTRICTED_CLIENT_ID);
			} else {
				requestBody += '&client_id=' + encodeURIComponent(oauth.CLIENT_ID);
			}
			
			return self.request(requestUrl, 'POST', {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				body: requestBody
			});
			
		}).then(function(res) {
			// Store the new access token
			var newData = res.json();
			
			return self.storeAuthorization({
				refresh_token: auth.refresh_token,
				instance_url: newData.instance_url,
				id: newData.id,
				login_server: auth.login_server,
				access_token: newData.access_token,
				_isReloadAsLimited: storeBackLimited
			});
			
		});
	}
	
});
