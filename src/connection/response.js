/**
	@class ConnectionResponse
	@classdesc Contains data from an HTTP request
	@param {string} message - The response message
	@param {Object} body - The response body
*/
var Class = require('level2-base').Class,
	JSONError = require('../base/errors').JSONError;

module.exports = Class.extend({
	
	// constructs given HTTP response information
	init: function(message, body) {
		this._msg = message;
		this.body = body;
		
		this.headers = this._msg.headers;
		this.statusCode = parseInt(this._msg.statusCode, 10);
		if(isNaN(this.statusCode)) {
			this.statusCode = null;
		}
	},
	
	/** Parses the HTTP response JSON string
		@memberof ConnectionResponse
		@instance
		@param {string} encoding - The type of encoding to use when parsing
		@return {Object} The parsed JSON
	*/
	json: function(encoding) {
		if(!this.body) return {};
		encoding = encoding || 'utf8';
		
		try {
			return JSON.parse(this.body.toString(encoding));
		} catch(err) {
			if(err instanceof SyntaxError !== true) throw err;
			throw new JSONError(err, this.body.toString(encoding));
		}
	}
	
});
