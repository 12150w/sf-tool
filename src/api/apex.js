/**
	@class ApexSoapAPI
	@classdesc SoapAPI backed by the apex.wsdl
	@extends SoapAPI
	@param {Connection} conn An instance of a project connection
	@param {Object} options An options hash
*/
var SoapAPI = require('./soap'),
	path = require('path');

module.exports = SoapAPI.extend({
	
	/** Gets the filepath to the apex WSDL file
		@memberof ApexSoapAPI
		@instance
		@return {string} A system file path
	*/
	getWsdlPath: function() {
		return path.join(__dirname, '..', '..', 'wsdl', this._conn.apiVersion, 'apex.wsdl');
	},
	
	/** Returns the SOAP endpoint to send SOAP envelopes to
		@memberof ApexSoapAPI
		@return {string} The endpoint URL
		@param {Object} auth The settings for a connection
	*/
	getEndpoint: function(auth) {
		return auth.instance_url + '/services/Soap/s/' + this._conn.apiVersion;
	}
	
});
