/**
	@class SoapAPI
	@classdesc Wraps a SOAP API
	@param {Connection} conn An instance of a project connection
	@param {Object} options An options hash
*/
var q = require('q'),
	soap = require('soap'),
	Class = require('level2-base').Class
	SOAPError = require('../base/errors').SOAPError,
	NotImplementedError = require('../base/errors').NotImplementedError;

module.exports = Class.extend({
	
	// init constructs the SOAP API wrapper
	init: function(conn, options) {
		this._conn = conn;
		this._opts = options || {};
		
		this._client = null;
		this._inRefresh = false;
		this._manualHeaders = [];
		
		this._wsdlPath = this.getWsdlPath();
	},
	
	/** Gets the filepath to the apex WSDL file
		@memberof SoapAPI
		@instance
		@return {string} Throws an error because this method cannot be called on the base class
	*/
	getWsdlPath: function() {
		throw new NotImplementedError('SoapAPI.getWsdlPath() must be overridden');
	},
	
	/** Returns the SOAP endpoint to send SOAP envelopes to
		@memberof SoapAPI
		@instance
		@return {string} Throws an error because this method cannot be called on the base class
	*/
	getEndpoint: function() {
		throw new NotImplementedError('SoapAPI.getEndpoint() must be overridden');
	},
	
	/** Sends a SOAP envelope to the SOAP endpoint
		@memberof SoapAPI
		@instance
		@return {Object} An object representing a SOAP response
		@param {string} action The name of the action to run
		@param {Object} args An options hash
	*/
	run: function(action, args) {
		var self = this;
		args = args || {};
		
		return this._initialize().then(function(client) {
			// Run the method
			if(client[action] == null) {
				throw new Error('Invalid action "' + action + '"');
			}
			
			return q.ninvoke(client, action, args);
			
		}).then(function(data) {
			// Process the response
			return {
				res: data[0] == null ? null : data[0].result,
				raw: data[1],
				headers: data[2]
			};
			
		}).fail(function(err) {
			// Look for server fault
			if(err.root == null) throw err;
			if(err.root.Envelope == null) throw err;
			if(err.root.Envelope.Body == null) throw err;
			if(err.root.Envelope.Body.Fault == null) throw err;
			
			// Check for reauth
			var parsedErr = new SOAPError(err.root.Envelope.Body.Fault);
			if(parsedErr.isSessionError() && self._inRefresh === false) {
				self._inRefresh = true;
				return self._conn.reloadToken().then(function() {
					self.reset();
					return self.run(action, args);
					
				}).then(function(res) {
					self._inRefresh = false;
					return res;
				});
			}
			
			throw parsedErr;
		});
	},
	
	/** Sets the session token header in the SOAP envelope
		@memberof SoapAPI
		@instance
		@return {Promise | null}
		@param {string} token The session token string
	*/
	setSession: function(token) {
		var self = this;
		
		return this._initialize().then(function(client) {
			client.clearSoapHeaders();
			client.addSoapHeader({
				'tns:SessionHeader': {'tns:sessionId': token}
			});
			
			self._manualHeaders.forEach(function(headerObject) {
				client.addSoapHeader(headerObject);
			});
		});
	},
	
	/** Sets the soap client to null
		@memberof SoapAPI
		@instance
		@return {null}
	*/
	reset: function() {
		this._client = null;
	},
	
	/** Sets a SOAP header
		@memberof SoapAPI
		@instance
		@return {Promise | null}
	*/
	setHeader: function(name, header) {
		var self = this;
		
		return this._initialize().then(function(client) {
			var headerObject = {};
			headerObject[name] = header;
			
			self._manualHeaders.push(headerObject);
			client.addSoapHeader(headerObject);
		});
	},
	
	/** Loads the WSDL and creates the client
		@memberof SoapAPI
		@instance
		@private
		@return {Promise | null}
	*/
	_initialize: function() {
		// Loads the WSDL file if needed
		if(this._client != null) return q(this._client);
		var auth,
			self = this;
		
		return self._conn.loadConfig().then(function(readConfig) {
			auth = readConfig;
			
			return q.nfcall(soap.createClient, self._wsdlPath, {
				endpoint: self.getEndpoint(auth),
				attributesKey: '$',
				valueKey: '_'
			});
			
		}).then(function(client) {
			self._client = client;
			return self.setSession(auth.access_token).then(function() {
				return client;
			});
			
		});
	}
	
});
