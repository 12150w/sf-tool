/**
	@class MetadataAPI
	@classdesc SoapAPI implementation for the metadata API
	@extends SoapAPI
	@param {Connection} conn An instance of a project connection
	@param {Object} options An options hash
*/
var SoapAPI = require('./soap'),
	path = require('path');

module.exports = SoapAPI.extend({
	
	/** Gets the filepath to the apex WSDL file
		@memberof MetadataAPI
		@instance
		@return {string} A system file path
	*/
	getWsdlPath: function() {
		return path.join(__dirname, '..', '..', 'wsdl', this._conn.apiVersion, 'metadata.wsdl');
	},
	
	/** Returns the SOAP endpoint to send SOAP envelopes to
		@memberof MetadataAPI
		@return {string} The endpoint URL
		@param {Object} auth The settings for a connection
	*/
	getEndpoint: function(auth) {
		return auth.urls.metadata.replace('{version}', this._conn.apiVersion);
	}
	
});
