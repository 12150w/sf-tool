/**
	@class Project
	@classdesc Represents a SalesForce project
	@param {string} folder The path to a project folder
*/
var Class = require('level2-base').Class,
	Settings = require('./yaml-settings'),
	q = require('q'),
	fs = require('fs-extra'),
	UIError = require('./errors').UIError,
	path = require('path');

module.exports = Class.extend({
	
	// constructs a SalesForce project
	init: function(folder) {
		this._folder = folder || process.cwd();
		this._settingsPath = path.join(this._folder, 'sf-project.yaml');
		
		// Public properties
		this.settings = new Settings(this._settingsPath);
		this.cache = new Settings(path.join(this._folder, '.sf-cache.yaml'));
		
		// Static setup
		this._hasInitialized = false;
		this._hasDestroyed = false;
	},
	
	/** Creates a project folder if it does not exist
		@memberof Project
		@instance
		@return {Promise | null}
	*/
	initialize: function() {
		// Sets up the contents (reading or writing)
		if(this._hasInitialized === true) return q();
		
		// Attempt to create the project folder
		return q.nfcall(fs.mkdir, this.path()).fail(function(err) {
			// Check for already exists error
			if(err.code !== 'EEXIST') throw err;
			
		});
	},
	
	/** Gets the project folder path
		@memberof Project
		@instance
		@return {string} The path to the project folder
	*/
	path: function() {
		// Returns the project folder path
		return this._folder;
	},
	
	/** Deletes the project folder
		@memberof Project
		@instance
		@return {Promise | null}
	*/
	destroy: function() {
		// Deletes the project contents
		var self = this;
		if(self._hasDestroyed === true) return q();
		
		return q.nfcall(fs.remove, self.path()).then(function() {
			self._hasDestroyed = true;
		});
	},
	
	/** Checks if the project settings exists
		@memberof Project
		@instance
		@return {Promise | boolean}
	*/
	exists: function() {
		return q.nfcall(fs.stat, this._settingsPath).then(function() {
			return true;
		}, function(err) {
			if(err.code === 'ENOENT') return false;
			else throw err;
		});
	},
	
	/** Checks if a component is in the project
		@memberof Project
		@instance
		@return {Promise | boolean}
		@param {Metadata} component The metadata component to check
	*/
	containsComponent: function(component) {
		var componentStoreName = component.getProjectStoreName();
		
		return this.settings.get(
			'package.' + component.getProjectTypeName()
		).then(function(metaList) {
			if(metaList == null) return false;
			
			var found = false;
			metaList.forEach(function(packageName) {
				if(found === true) return;
				
				if(packageName === componentStoreName) {
					found = true;
				}
			});
			
			return found;
		});
	},
	
	/** Adds a component to the project
		@memberof Project
		@instance
		@return {Promise | null}
		@param {Metadata} component The metadata component to add
	*/
	addComponent: function(component) {
		var self = this,
			listKey = 'package.' + component.getProjectTypeName()
		
		return self.containsComponent(component).then(function(exists) {
			// Check if it is already in the package
			if(exists === true) return;
			
			self.settings.get(listKey).then(function(metaList) {
				if(metaList == null) metaList = [];
				metaList.push(component.getProjectStoreName());
				
				return self.settings.set(listKey, metaList);
				
			});
			
		});
	},
	
	/** Removes the component from the settings
		@memberof Project
		@instance
		@return {Promise | null}
		@param {Metadata} component The metadata component to remove
	*/
	removeComponent: function(component) {
		// Check if the component is in the settings
		var self = this,
			listKey = 'package.' + component.getProjectTypeName();
		return this.containsComponent(component).then(function(exists) {
			if(!exists) {
				throw new UIError('Component not found in package');
			}
			
			// Get the list of components by type name
			self.settings.get(listKey).then(function(metaList) {
				if(!metaList) return;
				
				// Find the metadata in the list
				var index = metaList.indexOf(component.getProjectStoreName());
				if(index < 0) return;
				
				// Remove from the list
				metaList.splice(index, 1);
				
				// If there are no more components of this type, delete the list key
				if(metaList.length === 0) {
					// Get the whole settings package
					return self.settings.get('package').then(function(pkg) {
						
						// Delete the type from the package
						delete pkg[component.getProjectTypeName()];
						
						// Save changes
						return self.settings.set('package', pkg);
						
					});
				}
				
				// Save the list back to the file
				return self.settings.set(listKey, metaList);
				
			});
		});
	}
	
});
