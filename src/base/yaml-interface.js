/**
	@class YAMLInterface
	@classdesc An interface for serializing and parsing YAML
	@extends PersistenceInterface
	@param {Connection} conn An instance of a project connection
	@param {Object} options An options hash
*/
var PersistenceInterface = require('level2-base').PersistenceInterface,
	yaml = require('js-yaml');

module.exports = PersistenceInterface.extend({
	/** Converts a JavaScript object into a YAML string
		@memberof YAMLInterface
		@instance
		@param {Object} data - The object representation of the YAML to serialize
		@return {string} A YAML string
	*/
	serialize: function(data) {
		return yaml.safeDump(data);
	},
	
	/** Converts a YAML string into a JavaScript object
		@memberof YAMLInterface
		@instance
		@param {string} raw - The YAML string
		@return {Object} The YAML represented as an object
	*/
	parse: function(raw) {
		return yaml.safeLoad(raw);
	}
});