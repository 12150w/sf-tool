/**
	@module ui
*/
/* 
 * Interface for displaying data to users
 * 
 */
var chalk = require('chalk'),
	os = require('os'),
	cp = require('child_process'),
	errors = require('./errors'),
	q = require('q'),
	readline = require('readline'),
	UIError = errors.UIError;
	
var doPrinting = false;
module.exports = {
	/** spacer */
	spacer: '   ',
	
	/** Prints an error to the terminal
		@param {Object} err - An error object to print
	*/
	printError: function(err) {
		// Display error message
		var errorTitle = err.message;
		if(err instanceof UIError === false) errorTitle = err.name + ': ' + errorTitle;
		this.print(chalk.red.bold(errorTitle));
		
		// Display the stack trace
		if(err.stack != null && err instanceof UIError === false) {
			var trimmedStack = String(err.stack);
			trimmedStack = trimmedStack.substring(trimmedStack.indexOf('\n') + 1, trimmedStack.length);
			
			this.print(chalk.gray(trimmedStack));
		}
		
		// Display JSON source
		if(err instanceof errors.JSONError) {
			//TODO: Get specific details about the error
		}
		
	},
	
	/** Prints data to the terminal
		@param {string} line - The text to display
		@param {number} spacing - The number of spacing characters to write
	*/
	print: function(line, spacing) {
		if(doPrinting !== true) return;
		var space = '',
			_i;
		for(_i = 0; _i<spacing; _i++) {
			space += this.spacer;
		}
		
		// Split the lines
		line = String(line);
		line.split(/\n/).forEach(function(subLine) {
			process.stderr.write(space + subLine + os.EOL);
		});
	},
	
	/** Enables printing */
	enablePrint: function() {
		doPrinting = true;
	},
	
	/** Disables printing */
	disablePrint: function() {
		doPrinting = false;
	},
	
	/** Opens the url in the default browser */
	openBrowser: function(url) {
		var openCmd;
		
		// Get open command
		switch(os.platform()) {
			case 'darwin':
				openCmd = 'open';
				break;
			case 'win32':
				openCmd = 'start ""';
				break;
			default:
				openCmd = 'xdg-open';
		}
		
		// Run the command
		var safeUrl = url.replace(/"/g, '\\\"');
		return q.ninvoke(cp, 'exec', openCmd + ' "' + safeUrl + '"');
	},
	
	/** Requests text input from the user */
	prompt: function(prompt, opts) {
		opts = opts || {}
		
		// Merge options
		opts.required = opts.required === true ? true : false;
		opts.isBoolean = opts.isBoolean === true ? true : false;
		
		var reader = readline.createInterface({
			input: process.stdin,
			output: process.stdout,
			completer: opts.completer
		});
		
		// Prepare the question
		var promptText = prompt;
		if(opts.isBoolean) {
			opts.default = opts.default === true ? true : false;
			
			if(opts.default === true) promptText += ' (Y/n)';
			else promptText += ' (y/N)';
		}
		promptText += ': ';
		
		// Ask the question
		var askDefer = q.defer();
		reader.question(chalk.bold(promptText), function(answer) {
			reader.close();
			
			// Check for required
			if(!answer && opts.required) {
				this.print(chalk.red.bold('Must enter a value'));
				return askDefer.resolve(this.prompt(prompt, opts));
			}
			
			// Check for yes no question
			if(opts.isBoolean === true) {
				if(!answer) return askDefer.resolve(opts.default);
				var capAnswer = answer.toUpperCase();
				
				if(capAnswer === 'Y') return askDefer.resolve(true);
				if(capAnswer === 'N') return askDefer.resolve(false);
				
				this.print(chalk.red.bold('Must enter either "y" or "n"'));
				return askDefer.resolve(this.prompt(prompt, opts));
			}
			
			// Send back answer
			if(answer === '') answer = null;
			return askDefer.resolve(answer);
		});
		
		return askDefer.promise;
	},
	
	/** Makes the user verify an action */
	verify: function(question, options) {
		options = options || {};
		
		return this.prompt(question, {
			required: false,
			isBoolean: true,
			default: options.default == null ? true : options.default
			
		}).then(function(disposition) {
			// Throw an error if rejected
			if(disposition !== true) {
				throw new UIError('Action cancelled');
			}
			
		});
	},
	
	/** Displays the data contained in a deployment result */
	printDeployRes: function(res) {
		var self = this;
		
		if(res.success === true) {
			// Show changes
			this.print(chalk.bold.green('Deployment Successful'));
			
			if(res.details.componentSuccesses != null) {
				res.details.componentSuccesses.forEach(function(success) {
					if(success.fullName === 'package.xml') return;
					
					var msg = '';
					if(!!success.componentType) {
						msg += success.componentType + ' ';
					}
					msg += success.fullName;
					
					if(success.deleted === true) {
						self.print(chalk.yellow(msg + ' deleted'), 1);
					} else if(success.changed === true) {
						self.print(chalk.green(msg + ' changed'), 1);
					} else {
						self.print(chalk.gray(msg + ' no change'), 1);
					}
				});
			}
			
			// Show tests
			if(
				(res.details.componentSuccesses && res.details.componentSuccesses.length > 1)
				&& res.runTestsEnabled === true
			) {
				this.print('');
			}
			if(res.runTestsEnabled === true && res.details.runTestResult != null) {
				var testInfo = res.details.runTestResult,
					successes = testInfo.successes || [];
				
				successes.forEach(function(success) {
					var msg = 'Test: ' + success.name + '.' + success.methodName;
					
					self.print(chalk.gray(msg), 1);
				});
			}
			
		} else {
			// Show component errors
			this.print(chalk.bold.red('Deployment Failed'));
			
			if(res.details.componentFailures != null) {
				res.details.componentFailures.forEach(function(failure) {
					var msg = '';
					if(!!failure.componentType) {
						msg += failure.componentType + ' ';
					}
					msg += failure.fullName;
					
					self.print(chalk.red(msg), 1);
					
					var details = failure.problem;
					if(failure.lineNumber != null) {
						details = 'Line ' + failure.lineNumber + ': ' + details;
					}
					
					self.print(chalk.gray(details), 2);
				});
			}
			
			if(res.runTestsEnabled === true && res.details.runTestResult != null) {
				var testInfo = res.details.runTestResult,
					failures = testInfo.failures || [];
				
				if(
					(res.details.componentFailures != null && res.details.componentFailures.length > 0)
					&& failures.length > 1
				) {
					this.print('');
				}
				
				failures.forEach(function(failure) {
					self.print(chalk.red('Test: ' + failure.name + '.' + failure.methodName), 1);
					self.print(chalk.gray(failure.message), 2);
					self.print(chalk.gray(failure.stackTrace), 2);
				});
				
				if(testInfo.codeCoverageWarnings != null) {
					testInfo.codeCoverageWarnings.forEach(function(warn) {
						self.print(chalk.red('Coverage: ' + warn.name), 1);
						self.print(chalk.gray(warn.message), 2);
					});
				}
			}
			
		}
	}
	
};