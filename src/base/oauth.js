/**
	@module oauth
*/

module.exports = {
	/** OAuth client ID with full permissions */
	CLIENT_ID: '3MVG91ftikjGaMd_PRb1PObgLI36RpNb1QQZPO1rO3QHXzzqX_IP7vpKVbLZNf4uMK4XiBBCiDbbMMQ44Xuvn',
	
	/** OAuth client ID with login permissions */
	RESTRICTED_CLIENT_ID: '3MVG91ftikjGaMd_PRb1PObgLI3C0jiOwKso9ZoJwQrIiExS2od0r34j.diWABHpUQjelpdRJuFJEooTXoaRc'
};