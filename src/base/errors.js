/**
	@module errors
*/
var L2Error = require('level2-base').Error;

module.exports.NotImplementedError = L2Error.extend({
	name: 'NotImplementedError'
});

/**
	@class ArgumentError
	@classdesc An error for invalid arguments
	@extends L2Error
	@param {string} message Error message to display
	@param {string} arg An options hash
*/
module.exports.ArgumentError = L2Error.extend({
	// constructs the error
	init: function(message, arg) {
		this._super(message);
		this.arg = arg;
	},
	
	/**
		@name ArgumentError#name
		@type string
		@default 'ArgumentError'
	*/
	name: 'ArgumentError'
});

/**
	@class UIError
	@classdesc An error that occurs in the user interface
	@extends L2Error
*/
module.exports.UIError = L2Error.extend({
	/**
		@name UIError#name
		@type string
		@default 'UIError'
	*/
	name: 'UIError'
});

/**
	@class ElectronError
	@classdesc An error that occurs in an electron app
	@extends L2Error
*/
module.exports.ElectronError = L2Error.extend({
	/**
		@name ElectronError#name
		@type string
		@default 'ElectronError'
	*/
	name: 'ElectronError'
});

/**
	@class HTTPError
	@classdesc An error that occurs during an HTTP request
	@extends L2Error
	@param {Object} res The HTTP response object
*/
module.exports.HTTPError = L2Error.extend({
	/**
		@name HTTPError#name
		@type string
		@default 'HTTPError'
	*/
	name: 'HTTPError',
	
	// constructs the HTTP error
	init: function(res) {
		this._super(res.statusCode + '\n' + res.body.toString());
		
		this.res = res;
	}
});

/**
	@class JSONError
	@classdesc An error that occurs during JSON parsing
	@extends L2Error
	@param {Object} jsonErr The error when thrown by JSON
	@param {Object} body The text trying to be parsed by JSON.parse()
*/
module.exports.JSONError = L2Error.extend({
	/**
		@name JSONError#name
		@type string
		@default 'JSONError'
	*/
	name: 'JSONError',
	
	// constructs the JSON error
	init: function(jsonErr, body) {
		this._super(String(jsonErr));
		
		this.error = jsonErr;
		this.body = body;
	} 
});

/**
	@class SOAPError
	@classdesc An error that occurs when using a SOAP api
	@extends L2Error
	@param {Object} errData The response from a SOAP request
*/
module.exports.SOAPError = L2Error.extend({
	/**
		@name SOAPError#name
		@type string
		@default 'SOAPError'
	*/
	name: 'SOAPError',
	
	// constructs the SOAP error
	init: function(errData) {
		this._data = null;
		if(errData == null || errData.faultcode == null) {
			return this._super('Unknown Error: ' + JSON.stringify(errData));
		}
		this._data = errData;
		
		if(!!errData.faultstring) {
			return this._super(errData.faultstring);
		} else {
			return this._super(errData.faultcode);
		}
	},
	
	/** Checks to see if the error was due to an invalid session
		@memberof SOAPError
		@instance
		@return {boolean}
	*/
	isSessionError: function() {
		if(this._data == null) return false;
		
		if(this._data.faultcode.indexOf('INVALID_SESSION_ID') > -1) {
			return true;
		} else {
			return false;
		}
	}
});
