/**
	@module xml
	@desc Parses and generates XML
*/
var q = require('q'),
	xml2js = require('xml2js');

module.exports = {
	/** Default options for xml2js
		@private
	*/
	_options: {
		explicitArray: false,
		explicitRoot: false,
		attrkey: '$',
		charkey: '_'
	},
	
	/** A reference to an object that can build xml from an object
		@private
	*/
	_builder: new xml2js.Builder(this._options),
	
	/** Parses an XML string into a JavaScript object
		@param {string} xmlString - The xml in string form
		@return {Promise | Object} The parsed XML as an object
	*/
	parse: function(xmlString) {
		var parseDefer = q.defer();
		
		xml2js.parseString(xmlString, this._options, function(err, parsed) {
			if(err) return parseDefer.reject(err);
			parseDefer.resolve(parsed);
		});
		
		return parseDefer.promise;
	},
	
	/** Constructs an XML string from a JavaScript object
		@param {string} rootTag - The name of the root XML tag
		@param {Object} data - The object to convert into XML
		@return {string} The XML string
	*/
	build: function(rootTag, data) {
		var xmlString = this._builder.buildObject(data),
			xmlNS = ' xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"',
			xmlHeader = '<?xml version="1.0" encoding="UTF-8"?>';
		
		// Replace root tag
		var rootMatch = /^(<\?[^>]+\?>)\n<root>/.exec(xmlString);
		if(rootMatch != null) {
			xmlString = xmlString.replace(/^(<\?[^>]+\?>)\n<root>/, function(match) {
				return xmlHeader + '\n<' + rootTag + xmlNS + '>'
			});
			xmlString = xmlString.replace(/<\/root>$/, '</' + rootTag + '>');
		} else {
			xmlString = xmlString.replace(/<\?[^>]+\?>\n?/, '');
			xmlString = xmlHeader + '\n<' + rootTag + xmlNS + '>\n' + xmlString + '\n</' + rootTag + '>';
		}
		
		return xmlString;
	}
};