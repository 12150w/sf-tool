/*
 * YAML Settings
 *   Settings that persist data in the YAML format
 *
 */
/**
	@class YAMLSettings
	@classdescSettings that persist data in the YAML format
	@extends Settings
	@param {string} filePath The location of the settings file
*/
var Settings = require('level2-base').Settings,
	YAMLInterface = require('./yaml-interface');

module.exports = Settings.extend({
	
	// Construct using the YAML Interface
	init: function(filePath) {
		return this._super(filePath, new YAMLInterface());
	}
	
});
