/**
	@class Package
	@classdesc Contains Metadata components to move through the connection
	@param {Connection} conn An instance of a connection for a project
	@param {boolean} isProduction Denotes a production SalesForce connection
	@param {boolean} isDelete Denotes a package used to delete SalesForce metadata
*/
var Class = require('level2-base').Class,
	UIError = require('./errors').UIError,
	SubMetadata = require('../metadata/sub-metadata'),
	CompoundMetadata = require('../metadata/compound-metadata'),
	AdmZip = require('adm-zip'),
	JSZip = require('jszip'),
	chalk = require('chalk'),
	ui = require('./ui'),
	typesRegistry = require('../metadata/types/registry'),
	xml = require('./xml'),
	fs = require('fs'),
	yaml = require('js-yaml'),
	tty = require('tty'),
	os = require('os'),
	q = require('q'),
	path = require('path');

var Package = Class.extend({
	
	// constructs a package
	init: function(conn, isProduction, isDelete) {
		this._conn = conn;
		
		this._contents = {};
		this._contentListing = [];
		this._asyncInterval = 200;
		this._isProduction = isProduction === true;
		
		this._isDelete = !!isDelete;
		this._deleteBeforePack = null;
		this._deleteAfterPack = null;
	},
	
	/** Adds a metadata component to the package
		@memberof Package
		@instance
		@return {null}
		@param {Metadata} component The metadata component to add
	*/
	addComponent: function(component) {
		var componentKey = component.typeName + '.' + component.fullName;
		if(component.inFolders === true) componentKey += component.containingFolder;
		
		if(this._contentListing.indexOf(componentKey) > -1) return;
		
		if(this._contents[component.typeName] == null) {
			this._contents[component.typeName] = [];
		}
		
		component.isProduction = this._isProduction;
		this._contents[component.typeName].push(component);
		this._contentListing.push(componentKey);
	},
	
	/** Adds one or more metadata components listed in a YAML file
		@memberof Package
		@instance
		@return {null}
		@param {Object} setData An object representing components to add
	*/
	addSet: function(setData) {
		if(setData == null) return;
		
		for(var typeName in setData) {
			var SetType = typesRegistry.forName(typeName);
			
			for(var i=0; i<setData[typeName].length; i++) {
				var setComponent = new SetType(setData[typeName][i], this._conn.project);
				
				this.addComponent(setComponent);
			}
		}
	},
	
	/** Adds components by parsing cli arguments
		@memberof Package
		@instance
		@return {Promise | null}
		@param {Object} args An object representing command line arguments
		@param {Object} options An options hash
	*/
	addArgs: function(args, options) {
		var self = this;
		
		options = options || {};
		options.isDeleteSet = options.isDeleteSet === true ? true : false;
		
		return q.fcall(function() {
			
			// Save the entire package
			if((args == null || args.length < 1) && options.isDeleteSet !== true) {
				return self._conn.project.settings.get('package').then(function(packageSet) {
					if(packageSet == null) {
						throw new UIError('This project contains no components');
					}
					
					self.addSet(packageSet);
				});
			}
			
			// Add all components from the arguments
			var argPromises = [];
			args.forEach(function(arg) {
				var directMatch = /^([^:]+):([^:]+)$/.exec(arg);
				
				// Directly add a component
				if(directMatch !== null) {
					var DirectType = typesRegistry.forName(directMatch[1]);
					
					return self.addComponent(new DirectType(directMatch[2], self._conn.project));
				}
				
				// Add a component set
				return argPromises.push( q.nfcall(fs.readFile, arg, {
					encoding: 'utf8'
				
				}).then(function(setData) {
					self.addSet(yaml.safeLoad(setData));
					
				}) );
				
			});
			return q.all(argPromises);
			
		}).then(function() {
			// Add the delete components if needed
			if(options.isDeleteSet === true) return;
			var deleteAddPromises = [];
			
			if(options.deleteBefore != null) {
				self._deleteBeforePack = new Package(self._conn, self._isProduction);
				if(Object.prototype.toString.call(options.deleteBefore) !== '[object Array]') {
					options.deleteBefore = [options.deleteBefore];
				}
				
				deleteAddPromises.push(self._deleteBeforePack.addArgs(options.deleteBefore, {
					isDeleteSet: true
				}));
			}
			if(options.deleteAfter != null) {
				self._deleteAfterPack = new Package(self._conn, self._isProduction);
				if(Object.prototype.toString.call(options.deleteAfter) !== '[object Array]') {
					options.deleteAfter = [options.deleteAfter];
				}
				
				deleteAddPromises.push(self._deleteAfterPack.addArgs(options.deleteAfter, {
					isDeleteSet: true
				}));
			}
			
			return q.all(deleteAddPromises);
			
		});
	},
	
	/** Returns a list of metadata components in this package
		@memberof Package
		@instance
		@return {Metadata[]} An array of Metadata instances
	*/
	getComponents: function() {
		var componentList = [];
		
		for(var typeName in this._contents) {
			for(var i=0; i<this._contents[typeName].length; i++) {
				componentList.push(this._contents[typeName][i]);
			}
		}
		
		return componentList;
	},
	
	/** Sends a "retrieve" request to the Metadata API
		@memberof Package
		@instance
		@return {Promise | Object} An object representing the Metadata API response
	*/
	retrieve: function() {
		var self = this,
			pkgData = self._getPackageData();
		
		return self._conn.meta.run('retrieve', {
			retrieveRequest: {
				apiVersion: self._conn.apiVersion,
				unpackaged: pkgData
			}
			
		}).then(function(retData) {
			// Load the package data
			return self._waitForAsync(retData.res, 'checkRetrieveStatus');
			
		});
	},
	
	/** Pulls metadata components from SalesForce and then saves them to the hard drive
		@memberof Package
		@instance
		@return {Promise | Object[]} An array of objects representing metadata
	*/
	sync: function() {
		// Request the package
		var self = this,
			components = [];
		return this.retrieve().then(function(retrieve) {
			// Extract the components
			var zip = new AdmZip(new Buffer(retrieve.res.zipFile, 'base64')),
				extractPromises = [];
			
			for(var typeName in self._contents) {
				var typeList = self._contents[typeName];
				
				for(var i=0; i<typeList.length; i++) {
					var component = typeList[i];
					if(component.syncDisabled === true) {
						var typeName = component.typeNameAlias != null ? component.typeNameAlias : component.typeName;
						ui.print(chalk.yellow(typeName + ' ' + component.fullName + ' does not support syncing'));
						continue;
					}
					
					// Call a function so that a closure is created for the component variable
					components.push(component);
					extractPromises.push(component.extract(zip).fail(function(reason) {
						var typeName = this.contentType || this.typeName;
						ui.print(chalk.yellow(typeName + ' ' + this.getFullName() + ' could not be extracted from the package: ' + reason));
					}.bind(component)));
				}
			}
			
			return q.all(extractPromises);
			
		}).then(function(files) {
			
			var writePromises = [];
			for(var i=0; i<files.length; i++) {
				writePromises.push(components[i].write(files[i]).fail(function(reason) {
					var typeName = this.contentType || this.typeName;
					ui.print(chalk.yellow(typeName + ' ' + this.getFullName() + ' could not be written to the hard drive: ' + reason));
				}.bind(components[i])));
			}
			
			return q.all(writePromises);
			
		}).then(function() {
			
			return components;
			
		});
	},
	
	/** Adds all the components in the package to a zip file
		@memberof Package
		@instance
		@return {Promise | JSZip} In instance of JSZip
	*/
	buildPackage: function() {
		var self = this,
			pkgXml = xml.build('Package', this._getPackageData()),
			zip = new JSZip();
		
		// Create the zip file
		if(!this._isDelete) {
			zip.file('package.xml', new Buffer(pkgXml, 'utf8'));
			
			// Add destroy listings if needed
			if(this._deleteBeforePack != null) {
				var deleteBeforeXml = new Buffer(xml.build('Package', this._deleteBeforePack._getPackageData()), 'utf8');
				zip.file('destructiveChangesPre.xml', deleteBeforeXml);
			}
			if(this._deleteAfterPack != null) {
				var deleteAfterXml = new Buffer(xml.build('Package', this._deleteAfterPack._getPackageData()), 'utf8');
				zip.file('destructiveChangesPost.xml', deleteAfterXml);
			}
			
			// Add the package contents
			var compressPromises = [],
				compoundMapping = {},
				buildPromise;
			for(var typeName in this._contents) {
				for(var i=0; i<this._contents[typeName].length; i++) {
					var component = this._contents[typeName][i];
					
					// Check for sub metadata and defer to parent compression
					if(component instanceof SubMetadata) {
						if(compoundMapping[component._parent.typeName] == null) {
							compoundMapping[component._parent.typeName] = {};
						}
						if(compoundMapping[component._parent.typeName][component._parent.fullName] == null) {
							compoundMapping[component._parent.typeName][component._parent.fullName] = component._parent;
						}
						
						compoundMapping[component._parent.typeName][component._parent.fullName].addSubComponent(component);
						continue;
					}
					
					if(i===0) {
						var folderName = component.getFolderName();
						if(component instanceof CompoundMetadata) {
							folderName = component.folder;
						}
						
						zip.folder(folderName, new Buffer(0));
					}
					
					compressPromises.push(component.compress(zip));
				}
			}
			
			// Add the compound metadatas
			for(var typeName in compoundMapping) {
				for(var parentName in compoundMapping[typeName]) {
					compressPromises.push(compoundMapping[typeName][parentName].compress(zip));
				}
			}
			
			buildPromise = q.all(compressPromises);
		} else {
			// If this is a delete set, just add the package.xml and destructiveChanges.xml
			var blankPackage = xml.build('Package', { version: this._conn.apiVersion });
			
			zip.file('package.xml', new Buffer(blankPackage, 'utf8'));
			zip.file('destructiveChanges.xml', new Buffer(pkgXml, 'utf8'));
			
			buildPromise = q();
		}
		
		// Save the package
		return buildPromise.then(function() {
			// Get the zip contents
			return zip.generateAsync({
				compression: 'DEFLATE',
				type: 'nodebuffer'
			});
			
		});
	},
	
	/** Saves metadata components to SalesForce
		@memberof Package
		@instance
		@return {Promise | Object} An object representing the response from SalesForce
	*/
	save: function(options) {
		var self = this;
		return this.buildPackage().then(function(zipBuffer) {
			
			return self.send(zipBuffer, options);
			
		});
	},
	
	/** Sends a Metadata API request given a zip file representing the package contents
		@memberof Package
		@instance
		@return {Promise | Object} An object representing the response from SalesForce
		@param {Object} packageZip An object representing a zip file
		@param {Object} options An options hash
	*/
	send: function(packageZip, options) {
		var self = this,
			lastMessage = null;
		options = options || {};
		
		options.verifyOnly = options.verifyOnly === true ? true : false;
		options.rollback = options.rollback === true ? true : false;
		options.singlePackage = options.singlePackage !== false ? true : false;
		if(options.tests != null) {
			if(Object.prototype.toString.call(options.tests) !== '[object Array]') {
				options.tests = [options.tests];
			}
		}
		
		return q.fcall(function() {
			// Run the commit
			var deployOptions = {
				allowMissingFiles: false,
				autoUpdatePackage: false,
				checkOnly: options.verifyOnly,
				ignoreWarnings: false,
				performRetrieve: false,
				rollbackOnError: options.rollback,
				singlePackage: options.singlePackage
			};
			if(options.tests != null) {
				deployOptions.testLevel = 'RunSpecifiedTests';
				deployOptions.runTests = options.tests;
			}
			
			return self._conn.meta.run('deploy', {
				'zipFile': packageZip.toString('base64'),
				'deployOptions': deployOptions
			});
			
		}).then(function(asyncRes) {
			// Wait for the deployment to complete
			var spinIndex = 0;
			
			return self._waitForAsync(asyncRes.res,
				'checkDeployStatus'
			
			).progress(function(saveData) {
				// Publish save data message
				var compDone = false, testDone = false;
				
				// Check for component status
				if(saveData.numberComponentsTotal > 0) {
					var compPercent = (saveData.numberComponentsDeployed + saveData.numberComponentErrors) / saveData.numberComponentsTotal;
					compPercent = compPercent * 100;
					
					if(compPercent < 100) {
						return 'Saving Components: ' + Math.round(compPercent) + '%';
					} else {
						compDone = true;
					}
				}
				
				// Check for testing status
				if(saveData.numberTestsTotal > 0) {
					var testPercent = (saveData.numberTestsCompleted + saveData.numberTestErrors) / saveData.numberTestsTotal;
					testPercent = testPercent * 100;
					
					if(testPercent < 100) {
						return 'Running Tests: ' + Math.round(testPercent) + '%';
					} else {
						testDone = true;
					}
				} else if(saveData.runTestsEnabled === true) {
					return 'Saving Components: 100%';
				}
				
				// Check for complete
				if(
					compDone === true
					&& (testDone === true || saveData.runTestsEnabled !== true)
				) {
					return 'Finishing Deploy';
				}
				
				return 'Save Pending';
				
			}).progress(function(msg) {
				// Add formatting to message for TTY and timestamp
				
				if(!tty.isatty(process.stdout) || !tty.isatty(process.stderr)) {
					msg += os.EOL;
					if(lastMessage != msg) {
						lastMessage = msg;
					} else {
						return null;
					}
				}
				
				return (new Date()).toLocaleTimeString() + ': ' + msg;
			});
			
		});
	},
	
	/** Creates an object version of package.xml
		@memberof Package
		@instance
		@private
		@return {Object} An Object that matches the package.xml contents
	*/
	_getPackageData: function() {
		var pkg = {
			version: this._conn.apiVersion,
			types: []
		};
		
		for(var typeName in this._contents) {
			var pkgType = {
				name: typeName,
				members: []
			};
			
			for(var i=0; i<this._contents[typeName].length; i++) {
				var component = this._contents[typeName][i],
					pkgName = component.getFullName();
				
				if(component.inFolders === true) {
					pkgType.members.push(component.containingFolder + '/' + pkgName);
				} else {
					pkgType.members.push(pkgName);
				}
			}
			
			pkg.types.push(pkgType);
		}
		
		return pkg;
	},
	
	/** Monitors an async process
		@memberof Package
		@instance
		@private
		@return {Promise | null}
		@param {Object} asyncRes An object representing the initial metadata API response
		@param {string} statusMethod The SOAP action to use to monitor the status
	*/
	_waitForAsync: function(asyncRes, statusMethod) {
		// Returns a promise that resolves when the async process finishes
		var self = this,
			asyncDefer = q.defer();
		
		var checkResults = function() {
			self._conn.meta.run(statusMethod, {
				id: asyncRes.id,
				includeZip: true
				
			}).then(function(statusData) {
				// Publish status
				asyncDefer.notify(statusData.res);
				
				// Check for done
				if(statusData.res.done === true) {
					if(!!statusData.res.errorMessage) {
						return asyncDefer.reject(new UIError(statusData.res.errorMessage));
					}
					return asyncDefer.resolve(statusData);
				}
				
				// If not complete reschedule
				setTimeout(checkResults, self._asyncInterval);
				
			}, function(err) {
				// Send error up
				asyncDefer.reject(err);
				
			});
		};
		
		// Schedule the first status check
		checkResults();
		
		return asyncDefer.promise;
	}
	
});

module.exports = Package;
