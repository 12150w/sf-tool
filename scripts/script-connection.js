/*
 * Script Connection
 *   Connection for scripts
 *
 */
var Project = require('../src/base/project'),
	Connection = require('../src/connection/connection');

module.exports = new Connection(new Project(__dirname));
