/*
 * Build Types Scripts
 *   Rebuilds the types listing using the current API version
 *
 */
var conn = require('./script-connection'),
	chalk = require('chalk'),
	path = require('path'),
	fs = require('fs-extra'),
	q = require('q'),
	typeTemplates = require('../src/metadata/types/templates');

var compoundTypes = ['CustomObject', 'Workflow'];
var folderMapping = {
	'EmailTemplate': 'EmailFolder'
};

var SALESFORCE_TYPES_PATH = path.join(__dirname, '..', 'src', 'metadata', 'types', 'salesforce');

// Read the meta
conn.meta.run('describeMetadata', {
	apiVersion: parseInt(conn.apiVersion, 10)
	
}).then(function(describeData) {
	// Delete the old folder and it's contents
	return q.nfcall(fs.remove, SALESFORCE_TYPES_PATH).then(function() {
		
		// Create a new folder
		return q.nfcall(fs.mkdir, SALESFORCE_TYPES_PATH);
		
	}).then(function() {
		
		return describeData;
		
	});
	
}).then(function(describeData) {
	// Construct definitions
	var defPromises = [];
	
	describeData.res.metadataObjects.forEach(function(meta) {
		// Get the template
		var metaTemplate = typeTemplates.base;
		if(meta.metaFile === true) {
			metaTemplate = typeTemplates.content;
			defPromises.push(conn.meta.run('describeValueType', {
				type: '{http://soap.sforce.com/2006/04/metadata}' + meta.xmlName
				
			}));
		}
		
		// Build the definition
		var def = {
			typeName: meta.xmlName,
			extension: '.' + meta.suffix,
			folder: meta.directoryName
		};
		if(meta.inFolder === true) {
			def.inFolders = true;
			if(folderMapping[meta.xmlName] != null) {
				def.folderTypeName = folderMapping[meta.xmlName];
			}
		}
		if(meta.suffix == null) {
			def.extension = null;
		}
		
		// Check for child types
		var childPromise = q();
		if(meta.childXmlNames != null && compoundTypes.indexOf(meta.xmlName) > -1) {
			def.subTypes = {};
			metaTemplate = typeTemplates.compound;
			
			childPromise = conn.meta.run('describeValueType', {
				type: '{http://soap.sforce.com/2006/04/metadata}' + meta.xmlName
				
			}).then(function(typeDescribe) {
				// Find information on sub metadata
				var subCreatePromises = [];
				typeDescribe.res.valueTypeFields.forEach(function(fieldType) {
					if(meta.childXmlNames.indexOf(fieldType.soapType) < 0) return;
					if(fieldType.name === 'nameField' || fieldType.isNameField === true) return;
					
					console.log(chalk.gray('Generating ' + fieldType.soapType + ' (subtype)'));
					def.subTypes[fieldType.name] = fieldType.soapType;
					var subDef = {
							typeName: fieldType.soapType,
							folder: fieldType.name,
							parentTypeName: def.typeName
						},
						subDefPath = path.join(SALESFORCE_TYPES_PATH, fieldType.soapType + '.js'),
						subDefSrc = typeTemplates.sub.replace('{{META_INFO}}', JSON.stringify(subDef, null, '	'));
					
					subCreatePromises.push( q.nfcall(fs.writeFile, subDefPath, new Buffer(subDefSrc, 'utf-8')) );
				});
				
				return q.all(subCreatePromises);
				
			});
		}
		
		// Write the definition
		defPromises.push(childPromise.then(function() {
			var defPath = path.join(SALESFORCE_TYPES_PATH, meta.xmlName + '.js'),
				defData = String(JSON.stringify(def, null, '	')),
				defSrc = metaTemplate.replace('{{META_INFO}}', defData);
			console.log(chalk.gray('Generating ' + meta.xmlName));
			
			return q.nfcall(fs.writeFile, defPath, new Buffer(defSrc, 'utf-8'));
		}));
		
	});
	
	return q.all(defPromises);
}).done();
